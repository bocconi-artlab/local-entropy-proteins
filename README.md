# The native state of natural proteins optimises local entropy
This repository is divided in two parts. The first one contains a julia code for Monte Carlo simulations of lattice polymers. The second one contains scripts to perform molecular dynamics simulations with GROMACS and to perform analisys on the trajectories. 

Some results data of the simulations are included for both parts. 

* Lattice model
    * `results_phase_common_c` contains data to produce the phase diagram of the homopolymer. Figure 1 is produced with the script `fig1_phase_diagram_homopolymer.py`. The scripts `plot_traj_S.py` and `plot_traj_R.py` produce the example trajectories shown in the supplementary material.
    * `results_ref_common_c` contains data used to produce the confifurations used as reference to estimate the folding times. 
    * `results_GO` contains *only* the average trajectory for each of the reference configurations. Figure 2 is produced with the script `fig2_folding_time.py`.

* Molecular dynamics
    * `pdb_homopol` contains the reference configurations of the random decoys from which we build the go models. 
    * `pdb_go` and `pdb_homopol_go` contain the `.top` files that implement the Go models, produced by `smog2`. 
    * `runs` contains *only* the trajectories of the energy for every natural protein and every decoy, where the transient is already cutted out. 
    * `multiple_histogram/results` contains the entropy data used to produce figure 3. These results can also be reproduced from the data contains in `runs` by running `write_template.py` and then `launch_mhistrogram.sh`.

## Lattice-polymers Monte Carlo sampler
This is a Julia implementation of the common flip-and-rotation Monte Carlo algorithm for sampling configurations of lattice polymers.

The main feature is the support for interacting replicas, that allow to sample configurations with high local entropy.

### Basic usage
Modify the script `launch_mc.jl` by putting desired parameters into input variables of the simulation. Some of these variables are specified in a list: the program will cycle through every combination of parameters and execute in parallel with every available core (via the `pmap` instruction).

### General features
* different pre-set models: random walk (RW), hydro phobic/philic (HP), ferromagnetic (F), antiferromagnetic (AF)
* temperature annealing
* interacting replicas with support for annealing of the coupling constant
* different definitions of the distance function
* support for soft-core interactions, originally developed to increase the hardness over time during a simulation
* support for hooks (callbacks), used in particular to implement a GO model 

### Structure
* `lattice_polymer.jl` contains the structure `LatticePolymer` that stores the configuration of a lattice polymer (coordinates, monomer sequence, energy). A number of functions allow for changing the configuration and computing observables on it:
    * copy_state!
    * gen_config!
    * check_overlaps
    * check_good_config
    * find_contacts
    * compute_energy!
    * gen_d_matrix
    * gen_c_matrix
    * do_flip!
    * do_crankshaft!
    * do_rigid_rotation!
    * load_config!
    * gen_realization!
    * compute_endtoend
    * compute_gyration_radius
    * compute_distance
    * compute_common_contacts

* `mc_sampler.jl` contains the structure `MCSampler` that keeps track of current_state, proposed_state and the other informations necessary for a Monte Carlo sampling. A number of functions allow for proposing moves chosen at random from a hard-coded list that uses funcions defined inside `lattice_polymer.jl`.
    * restore_proposed_state!
    * propose_move!
    * accept_move!
    * metropolis_step!

* `replica_sampler.jl` contains the structure `ReplicaSampler` that contains Monte Carlo samplers of different replicas of the system. It adds some functions to handle the distance-based couplings. It also contains the function for the actual MC for loop.
    * compute_distance
    * compute_match_distance
    * compute_common_contacts
    * compute_matrix_distance 
    * compute_contact_distance
    * compute_endtoend (wrapper)
    * compute_gyration_radius (wrapper)
    * free_metropolis_step! (wrapper)
    * coupled_metropolis_step! 
    * do_monte_carlo
* `launch_mc.jl` is a script that launches the function `do_monte_carlo` with multiple set of parameters utilizing a parallel execution if multiple cores are available.

## Molecular dynamics simulations using GROMACS

This is a brief description of the pipeline we followed to measure local entropy with an all-atom model. 

1. In a directory called `pdb` we store the `.pdb` files of the native structures. The script `01_process_pdb.sh` processes those files with the tool `smog2` to create Go-model input files for `gromax`. Now we have Go models for native structure in the directory called `pdb_go`.
2. The script `02_homopol.sh` creates homopolymer input files for `gromax`.
3. The scripts `03_launch.homopol.optimiz.sh` and `04_launch.homopol.refold.sh` create the structures of the random decoys. The corresponding files are stored in a directory called `pdb_homopol`.
4. The script `05_process_pdb.homopol.sh` processes those files with the tool `smog2` to create Go-model input files for `gromax`. Now we have Go models for random decoys in the directory called `pdb_homopol_go`.
5. The scripts `launch.sh` `launch.homopol.sh` run the molecular-dynamics simulations respectively for the native Go models and for the decoys Go models.
