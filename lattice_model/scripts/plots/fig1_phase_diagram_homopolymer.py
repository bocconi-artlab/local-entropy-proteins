

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

from cycler import cycler

cm0=cm.rainbow_r
#cm0=cm.copper
nc = 5
plt.rc('axes', prop_cycle=(cycler('color', [cm0(c/(nc-1)) for c in range(nc)])))

cm0=cm.rainbow_r
#cm0=cm.bwr
fig = plt.figure(figsize=(4,3))
#fig.add_subplot(121)

################################################################################

path = "/Users/matteo5/random_heteropolymers/lattice_protein_monte_carlo/"
#1e-2 5e-2 1e-1 5e-1 1 5
filenames_S = [
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM1.00E-02_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM2.50E-02_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM5.00E-02_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM1.00E-01_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM2.50E-01_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM5.00E-01_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM1.00E+00_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM2.50E+00_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM5.00E+00_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM1.00E+01_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM2.00E+01_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM3.00E+01_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM4.00E+01_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM5.00E+01_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM6.00E+01_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM7.00E+01_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM8.00E+01_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM9.00E+01_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM1.00E+02_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM1.10E+02_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM1.20E+02_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM1.30E+02_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM1.40E+02_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM1.50E+02_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM1.60E+02_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM1.70E+02_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM1.80E+02_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM1.90E+02_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM2.00E+02_G1.0E+09_dG0.0E+00_GM0.0E+00/001/obs/r1.dat",
]

GM="1.0E+04"
filenames_R1 = [
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.00E-02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM2.50E-02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM5.00E-02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.00E-01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM2.50E-01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM5.00E-01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.00E+00_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM2.50E+00_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM5.00E+00_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM2.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM3.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM4.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM5.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM6.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM7.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM8.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM9.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.00E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.10E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.20E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.30E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.40E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.50E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.60E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.70E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.80E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.90E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM2.00E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
]

GM="1.5E+04"
filenames_R2 = [
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.00E-02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM2.50E-02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM5.00E-02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.00E-01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM2.50E-01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM5.00E-01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.00E+00_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM2.50E+00_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM5.00E+00_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM2.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM3.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM4.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM5.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM6.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM7.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM8.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM9.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.00E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.10E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.20E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.30E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.40E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.50E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.60E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.70E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.80E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.90E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM2.00E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
]

GM="2.0E+04"
filenames_R3 = [
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.00E-02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM2.50E-02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM5.00E-02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.00E-01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM2.50E-01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM5.00E-01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.00E+00_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM2.50E+00_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM5.00E+00_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM2.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM3.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM4.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM5.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM6.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM7.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM8.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM9.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.00E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.10E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.20E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.30E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.40E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.50E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.60E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.70E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.80E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.90E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM2.00E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
]

GM="4.0E+04"
filenames_R4 = [
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.00E-02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM2.50E-02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM5.00E-02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.00E-01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM2.50E-01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM5.00E-01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.00E+00_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM2.50E+00_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM5.00E+00_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM2.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM3.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM4.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM5.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM6.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM7.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM8.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM9.00E+01_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.00E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.10E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.20E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.30E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.40E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.50E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.60E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.70E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.80E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.90E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
f"results_phase_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM2.00E+02_G1.0E+09_dG0.0E+00_GM{GM}/001/obs/r1.dat",
]

# filenames_list = [filenames_S, filenames_R1,filenames_R2,filenames_R3]
# label_list = ["gamma 0e0","gamma 1e4","gamma 2e4","gamma 4e4"]
# beta_values = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200]

# filenames_list = [filenames_S, filenames_R1,filenames_R2,]
# label_list = ["gamma 0e0","gamma 1e4","gamma 2e4",]
# beta_values = [1e-2, 5e-2, 1e-1, 5e-1, 1, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200]

filenames_list = [filenames_S, filenames_R1,filenames_R2,filenames_R3,filenames_R4]
label_list = [r"$\gamma$=0",r"$\gamma$=1.0e4",r"$\gamma$=1.5e4",r"$\gamma$=2.0e4",r"$\gamma$=4.0e4"]
beta_values = [1e-2, 2.5e-2, 5e-2, 1e-1, 2.5e-1, 5e-1, 1, 2.5, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200]

################################################################################

def multi_hist(filenames, col, nbins=20, range=None):

    n_files = len(filenames)

    for i,filename in enumerate(filenames):
        #print(filename)
        data = np.loadtxt(path+filename)[500:,col]
        title = filename

        plt.hist(data, label=title,     bins=nbins, color=cm0(i/(n_files-1)), alpha=0.5,density=True,range=range)
        plt.hist(data, histtype='step', bins=nbins, color=cm0(i/(n_files-1))           ,density=True,range=range)

        #print(data.mean())

    return

################################################################################

def plot_averages(filenames, col, cut_idx=1000,label=""):

    #n_files = len(filenames)
    data_to_plot = []

    for i,filename in enumerate(filenames):
        data = np.loadtxt(path+filename)[cut_idx:,col]
        #print(len(data),filename)

#        data_to_plot.append([beta_values[i],  data.mean()])
        data_to_plot.append([beta_values[i],  data.mean(), data.std()])
        #print(beta_values[i],  data.mean(), data.std())

    data_to_plot = np.array(data_to_plot)
    x = data_to_plot[:,0]
    y = data_to_plot[:,1]
    yerr = data_to_plot[:,2]

    plt.plot(x,y,'o--', markersize=3, label=label)
    #plt.errorbar(x,y,fmt='o',yerr=yerr, markersize=3,capsize=3, label=label)
    plt.xscale("log")
    return

################################################################################

cut_idx_S=100
cut_idx=700

################################################################################

for i,filenames in enumerate(filenames_list):
    plot_averages(filenames, 7, cut_idx=cut_idx,label=label_list[i])

#plt.xlim((1e0,3e2))
plt.xlabel(r"Inverse Temperature $\beta$")
plt.ylabel("Radius of Gyration")
plt.legend(fontsize=8)

plt.tight_layout()
plt.savefig("images/phase_diagram.pdf")
plt.close()
################################################################################
