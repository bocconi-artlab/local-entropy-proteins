import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.ticker as ticker
from matplotlib.patches import Rectangle
from cycler import cycler

cm0=cm.rainbow_r
nc = 7
plt.rc('axes', prop_cycle=(cycler('color', [cm0(c/(nc-1)) for c in range(nc)])))

###########################################################################################
## data dist 

# data_dist = np.loadtxt("dist_vs_gamma.txt")

# x1 = data_dist[1:,0]    
# y1= data_dist[1:,1]

# # y1 = (1-np.array(y1)) * 140
# y1 = 1 - (1-np.array(y1)) * 140 / 82
###########################################################################################
## data tau

#table = np.loadtxt("traj_fit_data_reply.txt")
table = np.loadtxt("traj_fit_data.txt")

x2 = table[0:,0]
y2 = table[0:,2]
z2 = table[0:,3]

print(x2)

###########################################################################################
from cycler import cycler

# cm0=cm.rainbow_r
# nc = 2
# plt.rc('axes', prop_cycle=(cycler('color', [cm0(c/(nc-1)) for c in range(nc)])))
# ###########################################################################################
#matplotlib.rcParams.update({'font.size': 16})

fig = plt.figure(figsize=(4.5,3))
###########################################################################################
# left, width = 0.10, 0.3
# bottom, height = 0.20, 0.70
# rect_1 = [left, bottom, width, height]

# ax1 = fig.add_axes(rect_1)
ax1 = fig.add_subplot(111)
ax1bis = ax1.twinx()

ax1bis.plot(x2,z2,"o",color="C5")
ax1.plot(x2,y2,"^",color="C1")
ax1.set_zorder(1)               ## this puts ax1 correctly on the top layer
ax1.set_frame_on(False)

ax1.xaxis.set_major_formatter(ticker.FormatStrFormatter('%0.0e'))
ax1.set_xlabel(r'Coupling parameter $\gamma$')
ax1.set_ylabel(r'Median folding time $\tau$', color='C1')
ax1bis.set_ylabel(r'Median stability $f_\mathrm{eq}$', color='C5')

ax1.set_xticks([0, 1e4, 2e4, 3e4])
ax1.set_xticklabels(["0", "1e4", "2e4", "3e4"])
ax1.set_yticks([800,900,1000,1100,1200,1300])
ax1.set_yticklabels(["0.8e5","0.9e5","1.0e5","1.1e5","1.2e5","1.3e5"])

rect = Rectangle((8e3,600),5e3,5000,facecolor="orange", alpha=0.2)
ax1.add_patch(rect)
###########################################################################################
# spacing_2 = 0.16
# rect_2 = [left+width+spacing_2, bottom, width, height]
# ax2 = fig.add_axes(rect_2)

# ax2.plot(x1,y1,"v",color="C0")

# ax2.xaxis.set_major_formatter(ticker.FormatStrFormatter('%0.0e'))
# ax2.set_xlabel(r'Coupling parameter $\gamma$')
# #ax2.set_ylabel(r'Rep-cen common contacts')
# ax2.set_ylabel(r'Replica-center distance $d$')

# ax2.set_ylim(0.3, 1)

# ax2.set_xticks([0, 1e4, 2e4, 3e4])
# ax2.set_xticklabels(["0", "1e4", "2e4", "3e4"])

# rect = Rectangle((8e3,0.68),3e4,0.1,facecolor="orange", alpha=0.2)
# ax2.add_patch(rect)

# rect = Rectangle((8e3,0.3),5e3,10,facecolor="orange", alpha=0.2)
# ax2.add_patch(rect)
###########################################################################################
# spacing_3 = 0.01
# width_3 = 0.1
# rect_3 = [left+width+spacing_2+width+spacing_3, bottom, width_3, height]
# ax3 = fig.add_axes(rect_3, sharey=ax2)
# ax3.tick_params(axis="y", labelleft=False)
# ax3.tick_params(axis="x", labelbottom=False, bottom=False, labeltop=True, top=True)

# ax3.set_xlabel(r'$F(d)$')

# path = "/Users/matteo5/random_heteropolymers/"

# free_energy_files=[
#     [path+"md_flatness/mhistogram/GO/outfiles/g_GM0.0E+00_002.dat","C0",r"$\gamma$ 0.0E+00"],
#     [path+"md_flatness/mhistogram/GO/outfiles/g_GM0.0E+00_004.dat","C0",r"$\gamma$ 0.0E+00"],
#     [path+"md_flatness/mhistogram/GO/outfiles/g_GM0.0E+00_005.dat","C0",r"$\gamma$ 0.0E+00"],
#     [path+"md_flatness/mhistogram/GO/outfiles/g_GM0.0E+00_007.dat","C0",r"$\gamma$ 0.0E+00"],
#     [path+"md_flatness/mhistogram/GO/outfiles/g_GM0.0E+00_009.dat","C0",r"$\gamma$ 0.0E+00"],
#     [path+"md_flatness/mhistogram/GO/outfiles/g_GM0.0E+00_010.dat","C0",r"$\gamma$ 0.0E+00"],
#     [path+"md_flatness/mhistogram/GO/outfiles/g_GM0.0E+00_014.dat","C0",r"$\gamma$ 0.0E+00"],
# ]

# for i,item in enumerate(free_energy_files):

#     filename, color, label = item
#     if i%3!=0: label=None

#     print(filename)
#     data = np.loadtxt(filename)

#     #cmap = np.loadtxt(cmaps[i])

#     #T_list = [0.0090]
#     T_list = [0.00833]
#     #T_list = [0.0090, 0.0100, 0.0110]
#     styles = ["-","--","-."]

#     for j,T in enumerate(T_list):
#         y = data[:,0]
#         x = data[:,0] - T * data[:,1] 

#         plt.plot(x,y,styles[j],color=color,label=label, linewidth=0.5)

#     plt.xlim(0.85,1.00)

# rect = Rectangle((0.8,0.68),0.2,0.1,facecolor="orange", alpha=0.2)
# ax3.add_patch(rect)
# plt.text(0.95,0.85,"D",rotation="270")
# plt.text(0.95,0.73,r"$\ddag$",rotation="270")
# plt.text(0.95,0.50,"N",rotation="270")

###########################################################################################
plt.tight_layout()
#plt.show()
plt.savefig("images/folding_time.pdf")