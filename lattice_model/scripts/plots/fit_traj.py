

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm


################################################################################

#path = "/Users/matteo5/random_heteropolymers/lattice_protein_monte_carlo/"
path = "../../"

################################################################################


def gen_filename(γ,ref_idx,run_idx):
    filename = "results_GO/GM{:.1E}_{:03d}/GO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM1.20E+02_G1.0E+09_dG0.0E+00_GM0.0E+00/{:03d}/ref/overlap_r1.dat".format(γ,ref_idx,run_idx) 
    return filename

def gen_ref_list(γ,nref,nrun):
    ref_list = []
    for ref_idx in range(1,nref+1):
        run_list = [ gen_filename(γ,ref_idx,run_idx) for run_idx in range(1,nrun+1) ]
        ref_list.append(run_list)
    return ref_list

################################################################################

#################################################################################
def plot_av_traj(filenames, title="",end_idx = int(1e4), every = 1, f=1,color=None, plot_traj=True, save=False):

                        #results_GO/GM0.0E+00_001
    source_dir = "../../"+filenames[0][0:24] ## name of ref folder

    ####################################################################
    if save:    ## compute av traj
        print("SAVE:", filenames[0])

        n_files = len(filenames)
        data_to_plot = []

        data_to_plot = np.loadtxt(path+filenames[0])[:end_idx:every]
        n_points = data_to_plot.shape[0]
        data_to_plot = data_to_plot.reshape(1,n_points)
        

        for i in range(1,n_files):
            filename = filenames[i]

            data = np.loadtxt(path+filename)[:end_idx:every]
            #print(data.shape, filename)
            #plt.plot(data,label="")

            n_points = data.shape[0]
            data = data.reshape(1,n_points)

            data_to_plot = np.concatenate((data_to_plot, data), axis=0)
            

        av_traj = data_to_plot.mean(axis=0)
        #var_traj = data_to_plot.std(axis=0)

        x = np.array(list(range(every*f,(end_idx+1)*f,every*f)))
        y = av_traj
        #error = var_traj

        data_array = np.vstack((x,y))
        data_array = data_array.transpose()

        np.savetxt(source_dir+f"/av_traj.dat", data_array)
        #np.savetxt(source_dir+f"/av_traj_{title}.dat", data_array)
        
    ####################################################################
    else:   ## load data
        print("LOAD:", filenames[0])

        data_array = np.loadtxt(source_dir+f"/av_traj.dat")
        x = data_array[:,0]
        y = data_array[:,1]

    ####################################################################
    if plot_traj:
        if color == None:
            plt.plot(x,y,label=title, linewidth=0.5)
        else:
            plt.plot(x,y,label=title, linewidth=0.1, c=color)

        #plt.fill_between(x, y-error, y+error, alpha=0.2)
    ####################################################################

    return x, y

################################################################################


################################################################################

def func(x, a, b, c):
     return a * (1-np.exp(-x/b)) + c

################################################################################
################################################################################

def fit_and_hist1(ref_list_A, nref, func, bool_save=1, title='', color='C0', anchor=1, plot_traj=True):

    out1_list_A = []
    out2_list_A = []
    out3_list_A = []

    p0 = [0.8, 200, 0.0]
    #p0 = [0.7, 1000.0, 0.0, 0.3, 50.0]

    bounds = (0, [1.0,np.inf,1.0])
    #bounds = (0, [1,np.inf,1,1,np.inf])
    do_save = bool(bool_save) ## if false loads form previously computed av traj

    fig.add_subplot(220+anchor+0)
    for n in range(nref):

        #######################################################################################
        x, yS = plot_av_traj(ref_list_A[n], title=title,color=color,plot_traj=plot_traj, save=do_save)

        xcut = 100

        popt, _ = curve_fit(func, x[xcut:], yS[xcut:], p0=p0,maxfev=5000, bounds=bounds)
        out1_list_A.append(popt[0])
        out2_list_A.append(popt[1])
        out3_list_A.append(popt[2]+popt[0])

        if n==0: label="S"
        #if plot_traj: plt.plot(x, func(x,*popt),"--",linewidth=1, color="C0",label=label)
        #######################################################################################
        #######################################################################################

    fig.add_subplot(220+anchor+1)
    plt.hist((out2_list_A),color=color,                bins=20,range=(0,7000),density=True,alpha=0.5,label=title)
    plt.hist((out2_list_A),color=color,histtype='step',bins=20,range=(0,7000),density=True,alpha=1.0)
    plt.xlabel("tau")
    plt.grid(linewidth=0.3)
    plt.legend()

    fig.add_subplot(220+anchor+2)
    plt.hist((out3_list_A),color=color,                bins=20,range=(0.6,1),density=True,alpha=0.5,label=title)
    plt.hist((out3_list_A),color=color,histtype='step',bins=20,range=(0.6,1),density=True,alpha=1.0)
    plt.xlabel("ampl")
    plt.grid(linewidth=0.3)
    plt.legend()

    return np.median(out1_list_A), np.median(out2_list_A), np.median(out3_list_A)
    #return np.mean(out1_list_A), np.mean(out2_list_A), np.mean(out3_list_A)

################################################################################
from cycler import cycler
from scipy.optimize import curve_fit
################################################################################

nrun=40
nref=60

nrun_list =  [40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40]
save_list =  [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
#save_list =  [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

gamma_list = [5.0e2, 2.0e3, 2.5e3, 3.0e3, 5e3,6e3,7e3,8e3,9e3,1e4,1.1e4,1.2e4,1.3e4,1.4e4,1.5e4,2e4,3e4]
label_list = [5.0e2, 2.0e3, 2.5e3, 3.0e3, 5e3,6e3,7e3,8e3,9e3,1e4,1.1e4,1.2e4,1.3e4,1.4e4,1.5e4,2e4,3e4]
color_list = [f'C{n}' for n in range(0,len(gamma_list))]

################################################################################
cm0=cm.rainbow_r
nc = len(gamma_list)
plt.rc('axes', prop_cycle=(cycler('color', [cm0(c/(nc-1)) for c in range(nc)])))

fig = plt.figure(figsize=(12,8))

################################################################################
outname = "traj_fit_data.txt"
outfile = open(outname, 'w+')

for i,gamma in enumerate(gamma_list):
    ref_list = gen_ref_list(gamma,nref,nrun_list[i])
    averages = fit_and_hist1(ref_list, nref, func, anchor=1,title=label_list[i],color=color_list[i],bool_save=save_list[i])

    print(gamma, *averages, file=outfile)

outfile.close()
################################################################################
plt.tight_layout()
#plt.show()
plt.savefig("images/fit_traj.pdf")
