import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from cycler import cycler

########################################################################################
cm0=cm.rainbow_r
nc = 7
plt.rc('axes', prop_cycle=(cycler('color', [cm0(c/(nc-1)) for c in range(nc)])))
########################################################################################

home="../../"

dirs=[
"results_phase_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM1.00E+02_G1.0E+09_dG0.0E+00_GM0.0E+00/",
]

items=[
["001/obs/r1.dat","C1","single"   , 1.0],
]

########################################################################################

for dir in dirs:

    print(dir)
    fig = plt.subplots(figsize=(4,3))

    for item in items:
        print(item)
        file, color, label, linewidth = item
        traj = np.loadtxt(home+dir+file)
        mc_step = traj[:,0]
        gyr = traj[:,7]
        plt.plot(mc_step, gyr, color=color, label=label, linewidth=linewidth)

    ########################################################################################

    beta = dir[-40:-30]
    gamma = dir[-10:-1]

    plt.legend()
    plt.xlabel("MC steps")
    plt.ylabel("Gyration radius")

    plt.tight_layout()
    plt.subplots_adjust(top=0.90)
    plt.title(r"$\beta = $" + f"{beta[2:]}" + r"; $\gamma = $" + f"{gamma[2:]}")

    plt.savefig(f"images/traj_{beta}_{gamma}.pdf")
    plt.close()

    ########################################################################################

########################################################################################