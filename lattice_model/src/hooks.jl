
################################################################################

function setup_ref_dir(rsampler::ReplicaSampler)

    results_dir = rsampler.results_dir
    ref_dir = joinpath(@__DIR__,results_dir, "ref")

    rm(ref_dir, force=true, recursive=true) ## If force=true is passed, a non-existing path is not treated as error

    println("setup_ref_dir: cleaned dir")

    return
end

################################################################################

# function dist_from_ref(ref_state::LatticePolymer, rsampler::ReplicaSampler; do_print=true, do_save=true)

#     dist_from_ref = compute_distance(rsampler.replica[1].current_state, ref_state)

#     if do_print
#         println("dist_from_ref: ", dist_from_ref)
#     end

#     if do_save
#         results_dir = rsampler.results_dir
#         ref_dir = joinpath(@__DIR__,results_dir, "ref")
#         mkpath(ref_dir)

#         filename_ref = "dist_from_ref.dat" ## put this outside for and write here a for i in 1:rsampler.n_replicas
#         outfile_ref = joinpath(ref_dir, filename_ref)
#         #rm(outfile_ref)
#         f = open(outfile_ref, "a")
#         println(f, "$(dist_from_ref)")
#         close(f)
#     end

#     return dist_from_ref
# end

################################################################################

function overlap_with_ref(ref_state::LatticePolymer, rsampler::ReplicaSampler, r1::Int64;  which_state="current", divide_by=1,do_print=false, do_save=false)

    if which_state=="proposed"
        overlap = compute_common_contacts(rsampler.replica[r1].proposed_state, ref_state)
    elseif which_state=="current"
        overlap = compute_common_contacts(rsampler.replica[r1].current_state, ref_state)
    end

    if do_print
        println("overlap_with_ref r$r1: ", overlap/divide_by)
    end

    if do_save
        results_dir = rsampler.results_dir
        ref_dir = joinpath(@__DIR__,results_dir, "ref")
        mkpath(ref_dir)

        filename_ref = "overlap_r$(r1).dat" ## put this outside for and write here a for i in 1:rsampler.n_replicas
        outfile_ref = joinpath(ref_dir, filename_ref)
        #rm(outfile_ref)
        f = open(outfile_ref, "a")
        println(f, "$(overlap/divide_by)")
        close(f)
    end

    return overlap
end

################################################################################
