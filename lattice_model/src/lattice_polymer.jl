#using ExtractMacro
#using Random
#using LinearAlgebra

################################################################################
mutable struct LatticePolymer

    n_dim       ::Int64
    n_monomers  ::Int64
    versors     ::Matrix{Int64}    ## versors for each 2*D possible turn
    realization ::Vector{Int64}    ## actual realization of turns
    coordinates ::Matrix{Int64}    ## correspondent sparial config

    model_name          ::AbstractString         ## name of some traditional set of interactions
    n_types             ::Int64          ## number of different types of monomer
    sequence            ::Vector{Int64}  ## sequence of monomer types
    interaction_matrix  ::Matrix{Int64}  ## n_types x n_types interaction matrix

    use_energy  ::Bool
    energy      ::Float64

    function LatticePolymer(n_dim::Int64,n_monomers::Int64, model_name::AbstractString; sequence::Union{Vector{Int64},Nothing}=nothing)

        ## FIST initialize the sequence part ###################################

        ## Self-Avoiding Random Walk or
        ## Go model, because we just include an additional term in coupled_metropolis_step
        if model_name == "RW" || model_name == "GO"
            n_types = 1
            interaction_matrix = zeros(Float64,n_types,n_types)

        ## Homopolymer
        elseif model_name == "HOMO"
            n_types = 1
            interaction_matrix = -1.0 .* ones(Float64,n_types,n_types)

        ## HP model
        elseif model_name == "HP"
            n_types = 2
            interaction_matrix = zeros(Float64,n_types,n_types)
            interaction_matrix[1,1] = -1.0

        ## symmetrized HP model (copolymer) [Ferromagnet]
        elseif model_name == "F"
            n_types = 2
            interaction_matrix = ones(Float64,n_types,n_types)
            interaction_matrix[1,2] = -1.0
            interaction_matrix[2,1] = -1.0

        ## charged model (polyampholyte) [AntiFerromagnet]
        elseif model_name == "AF"
            n_types = 2
            interaction_matrix = ones(Float64,n_types,n_types)
            interaction_matrix[1,1] = -1.0
            interaction_matrix[2,2] = -1.0
        end

        ## initialize sequence to random if no input sequence is provided
        if sequence===nothing
            sequence = rand(1:n_types, n_monomers)
        end
        ## TODO put here a check that the provided sequence has the correct range of types and number of monomers

        use_energy = any(!iszero, interaction_matrix)

        energy = use_energy ? 0.0 : Inf

        ## THEN initialize the config part ####################################
        versors = zeros(Int64,(n_dim,2*n_dim))  ## 2*D possible turns in D dimensions

        ## generate all the possible turns
        for t in 1:n_dim
            versors[t,t] = 1
            versors[t,t+n_dim] = -1
        end

        n_versors = n_monomers - 1
        realization = ones(Int64, n_versors)             ## the first turn is set to 1
        realization[2:end] = rand(1:n_dim, n_versors-1)  ## subset of turns that guarantees a good initializarion
                                                         ## (the full set of turns goes to 2*n_dim)

        coordinates = zeros(Int64,(n_dim,n_monomers))

        chain = new(n_dim, n_monomers, versors, realization, coordinates,
                    model_name, n_types, sequence, interaction_matrix,
                    use_energy, energy)
        gen_config!(chain)  ## not mandatory inplace but doesnt use additional memory

        return chain
    end

end

function copy_state!(dest::LatticePolymer, src::LatticePolymer)
    #copy!(dest.realization, src.realization)
    #copy!(dest.coordinates, src.coordinates)
    dest.realization = copy(src.realization)
    dest.coordinates = copy(src.coordinates)
    dest.energy = src.energy

    # println(dest.realization === src.realization)
    # println(dest.coordinates === src.coordinates)
    # println(dest.energy === src.energy)
    # src.energy = src.energy + 1
    # println(dest.energy," ",src.energy)
end

################################################################################

function gen_config!(chain::LatticePolymer)
    @extract chain : n_dim n_monomers realization versors coordinates

    @inbounds for n in 2:n_monomers
        versor_idx = realization[n-1]
        # coordinates[:,n] .= coordinates[:,n-1] .+ versors[:,versor_idx]
        for d = 1:n_dim
            coordinates[d,n] = coordinates[d,n-1] + versors[d,versor_idx]
        end
    end

    return coordinates
end

################################################################################

function check_overlaps(chain::LatticePolymer)
    config = chain.coordinates
    n_overlapping_monomers = chain.n_monomers - size(unique(config, dims=2), 2)
    return  n_overlapping_monomers
end

################################################################################

function check_good_config(chain::LatticePolymer)
    n_overlapping_monomers = check_overlaps(chain)
    return  n_overlapping_monomers == 0
end

################################################################################

function find_contacts(chain::LatticePolymer)
    @extract chain : n_dim n_monomers

    config = chain.coordinates
    contacts_list = NTuple{2,Int64}[]

    @inbounds for i in 1:n_monomers
        conf_i = @view(config[:,i])
        for j = (i+3):n_monomers
            conf_j = @view(config[:,j])
            z = n_dim - 1
            u = 1
            for d = 1:n_dim
                diff = abs(conf_i[d] - conf_j[d])
                if diff == 0
                    z -= 1
                elseif diff == 1
                    u -= 1
                    u < 0 && break
                else
                    break
                end
            end
            z == 0 && u == 0 && push!(contacts_list, (i,j))
        end
    end

    return contacts_list
end

################################################################################

function compute_energy!(chain::LatticePolymer)
    chain.use_energy || return 0.0
    @extract chain : n_monomers sequence interaction_matrix

    contacts = find_contacts(chain)

    tot_energy = 0
    for (i,j) in contacts
        type_i, type_j = sequence[i], sequence[j]

        contact_energy = interaction_matrix[type_i,type_j]

        tot_energy += contact_energy
    end

    chain.energy = tot_energy

    return tot_energy
end

################################################################################

function gen_d_matrix(chain::LatticePolymer; dist_to_contact=false)
    @extract chain : n_dim n_monomers

    config = chain.coordinates
    d_matrix = zeros(Float64,(n_monomers,n_monomers))

    @inbounds for i in 1:n_monomers
        coord_i = @view(config[:,i])

        @inbounds for j in i+1:n_monomers
            coord_j = @view(config[:,j])

            d_matrix[i,j] = norm(coord_i-coord_j)
            d_matrix[j,i] = norm(coord_i-coord_j)
        end
    end

    dist_to_contact && (d_matrix .= d_matrix .< 1)

    return Symmetric(d_matrix)  ## is it better to use UnitUpperTriangular ?
end

################################################################################

gen_c_matrix(chain::LatticePolymer) = gen_d_matrix(chain::LatticePolymer, dist_to_contact=true)

################################################################################

"""
Swap the values of a pair of neighbouring chain vectors.
"""
function do_flip!(chain::LatticePolymer; offset = 1)
    @extract chain : n_monomers realization

    n_versors = n_monomers - 1
    max_idx = n_versors - offset
    #idx_list = randperm(max_idx) ## exclude the last one, since we swap (idx) and (idx+1)
    idx_list = shuffle(2:max_idx) ## exclude the first one, since now we fix it

    t = 1
    idx = idx_list[t]
    versor_1 = realization[idx]
    versor_2 = realization[idx+offset]

    while (versor_1 == versor_2) && (t ≤ (max_idx-1))
        #println("silly $(offset==1 ? "flip" : "crank") ($idx <-> $(idx+offset)), try next")
        t += 1
        idx = idx_list[t]
        versor_1 = realization[idx]
        versor_2 = realization[idx+offset]
    end

    realization[idx] = versor_2
    realization[idx+offset] = versor_1

    return idx
end

################################################################################

"""
Swap the values of a pair of chain vectors that are next-nearest
neighbours.
"""
do_crankshaft!(chain::LatticePolymer) = do_flip!(chain; offset=2)

################################################################################

"""
Rotate a chain vector clockwise or counter-clockwise (needs work in d>2);
all the following chain vectors are rotated accordingly.
"""
function do_rigid_rotation!(chain::LatticePolymer)
    @extract chain : n_dim n_monomers realization

    n_versors = n_monomers - 1
    max_idx = n_versors
    idx = rand(2:max_idx)   ## exclude first monomer because we fix the first turn

    direction = rand(-1:2:1)
    dmax = 2 * n_dim

    realization[idx:end] .= mod1.((realization[idx:end] .+ direction), dmax)

    return idx
end

################################################################################

function load_config!(filename::AbstractString, chain::LatticePolymer)
    @extract chain : coordinates n_dim n_monomers

    loaded_config = readdlm(filename,' ', Int64)[:,1:(end-1)] # last colums is for monomer ID
    loaded_config = loaded_config'

    @assert n_dim == size(loaded_config)[1]
    @assert n_monomers == size(loaded_config)[2]

    coordinates .= loaded_config

    return coordinates
end

################################################################################

function load_sequence!(filename::AbstractString, chain::LatticePolymer)
    @extract chain : n_monomers sequence

    loaded_sequence = readdlm(filename,' ', Int64)[:,end] # last colums is for monomer ID

    @assert n_monomers == size(loaded_sequence)[1]

    sequence .= loaded_sequence

    return sequence
end

################################################################################

function load_config_and_sequence!(filename::AbstractString, chain::LatticePolymer)
    @extract chain : coordinates n_dim n_monomers sequence

    file = readdlm(filename,' ', Int64)
    loaded_config = file[:,1:(end-1)] # last colums is for monomer ID
    loaded_config = loaded_config'
    loaded_sequence = file[:,end] # last colums is for monomer ID

    @assert n_dim == size(loaded_config)[1]
    @assert n_monomers == size(loaded_config)[2]

    coordinates .= loaded_config
    sequence .= loaded_sequence

    return coordinates, sequence
end

################################################################################

function gen_realization!(chain::LatticePolymer)
    @extract chain : coordinates realization versors n_monomers n_dim

    n_versors = n_monomers - 1
                                       ## the first turn is set to 1
    for m in 2:n_versors               ## gen the rest of the realization
        Δcoordinates = coordinates[:,m+1] .- coordinates[:,m]

        for k in 1:2*n_dim             ## cycle to find the proper versor
            if versors[:,k] == Δcoordinates
                realization[m] = k
                break
            end
        end
    end

    return
end

################################################################################

function compute_endtoend(chain::LatticePolymer)
    @extract chain : n_monomers

    coordinates_r1 = chain.coordinates
    return norm(coordinates_r1[:,n_monomers])
end

################################################################################

function compute_gyration_radius(chain::LatticePolymer)
    @extract chain : n_monomers

    coordinates_r1 = chain.coordinates

    gyration_radius = 0.0

    for i in 1:n_monomers
        for j in (i+1):n_monomers
            gyration_radius += norm(coordinates_r1[:,i] - coordinates_r1[:,j])^2
        end
    end

    return sqrt(gyration_radius) / n_monomers
end

################################################################################

function compute_distance(chain_1::LatticePolymer, chain_2::LatticePolymer )
    @extract chain_1 : n_dim n_monomers

    coordinates_1 = chain_1.coordinates
    coordinates_2 = chain_2.coordinates

    dist = 0.0
    @inbounds for m in 1:n_monomers
        coord1, coord2 = @view(coordinates_1[:,m]), @view(coordinates_2[:,m])
        dist1 = 0.0
        @simd for d = 1:n_dim
            dist1 += (coord1[d] - coord2[d])^2
        end
        dist += √dist1
    end

    return dist
end

################################################################################

function compute_common_contacts(chain_1::LatticePolymer, chain_2::LatticePolymer)

    contacts_r1 = find_contacts(chain_1)
    contacts_r2 = find_contacts(chain_2)

    common_contacts = intersect(contacts_r1,contacts_r2)
    n_common_contacts = length(common_contacts)

    return n_common_contacts
end

################################################################################
