using Distributed
@everywhere using Printf
@everywhere begin
    using Random
    using Statistics
    using DelimitedFiles
    using ArgParse

    ################################################################################
    include(joinpath(@__DIR__,"replica_sampler.jl"))
    #include("replica_sampler.jl")
    #include("src/replica_sampler.jl")
    MC = LatticePolymerSampler

    ############################################################################

    fix_seed = false

    if fix_seed
        seed = 42
        println("REMINDER: seed fixed to $seed")
        Random.seed!(seed)
    end

    ############################################################################

    n_dim = 3
    n_monomers = 70
    model_name = "GO"
    n_replicas = 1

    ann_timescale = 100
    n_mc_steps = Int(1e7)
    n_obs_prints     = 1e4
    n_config_prints  = 10


    do_soft_core = false
    use_distance="rmsd"

    do_slow_init=true

    β_list = [1e+9]; Δβ_list = [0.0]; β_max_list =[10 20 30 40 50 60 70 80 90 100 110 120 130 140 150 160 170 180 190 200]
    γ_list = [1e+9]; Δγ_list = [0.0]; γ_max_list = [ 0 ]

    n_runs = 1
    run_index_list = collect(1:n_runs)


    list_S = [ 
        "results_ref_common_c/HOMO_3D_N70_R1_B1.0E+09_dB0.0E+00_BM1.00E+02_G1.0E+09_dG0.0E+00_GM0.0E+00/$(@sprintf("%03d",i))/config"
        for i in 1:3 
    ]
 
    list_R = [ 
        "results_ref_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.00E+02_G1.0E+09_dG0.0E+00_GM1.0E+04/$(@sprintf("%03d",i))/config"
#results_ref_common_c/HOMO_3D_N70_R4_B1.0E+09_dB0.0E+00_BM1.20E+02_G1.0E+09_dG0.0E+00_GM1.0E+04/$(@sprintf("%03d",i))/config
        for i in 1:3
    ]

    reference_dir_list = vcat(list_S,list_R)
end

pmap([Dict(:run_index=>run_index, :β=>β, :Δβ=>Δβ, :β_max=>β_max, :γ=>γ, :Δγ=>Δγ, :γ_max=>γ_max,:reference_dir=>reference_dir)
    for run_index in run_index_list
    for  β in  β_list
    for Δβ in Δβ_list
    for  γ in  γ_list
    for Δγ in Δγ_list
    for β_max in β_max_list
    for γ_max in γ_max_list
    for reference_dir in reference_dir_list  ## this last cycle is the first to be executed by workers
        ]) do kw

    #println(kw)
    #println(kw[:reference_dir])
    reference_dir = kw[:reference_dir]
    pop!(kw, :reference_dir)
    ############################################################################
    results_dir="../results_GO_phase/"*reference_dir[86:94]*"_"*reference_dir[96:98]
    ## load config and sequence from a previous run
    reference_file = joinpath(reference_dir, "00010_r1.dat")
    reference_state = MC.LatticePolymer(n_dim,n_monomers,model_name)
    ref_configuration = MC.load_config!(reference_file, reference_state)
    sequence = nothing

    global n_contacts_ref = size(MC.find_contacts(reference_state))[1]

    ############################################################################
    ## define hooks that depend on the reference state

    ## hook to add to the (empty) energy the go term
    function go_energy(rsampler, r1)
        E1 = MC.overlap_with_ref(reference_state, rsampler, r1 , which_state="current" )
        E2 = MC.overlap_with_ref(reference_state, rsampler, r1 , which_state="proposed" )
        return -1.0 * (E2-E1)
    end

    ## hook to save overlap with target on a file
    write_go_overlap(rsampler, r1) = MC.overlap_with_ref(reference_state, rsampler, r1, divide_by=n_contacts_ref, do_save=true)  
    ## hook to print overlap status
    print_go_overlap(rsampler, r1) = MC.overlap_with_ref(reference_state, rsampler, r1, divide_by=n_contacts_ref, do_print=true)     
    ############################################################################

    hooks_list_init=[MC.setup_ref_dir]
    hooks_list_status=[print_go_overlap]
    hooks_list_obs=[write_go_overlap]
    hooks_list_config=[]
    additional_energy_list=[go_energy]

    ############################################################################
    println(n_monomers," ",kw)

    rsampler = MC.do_monte_carlo( n_mc_steps,
                        sequence=sequence,
                        n_dim=n_dim, model_name=model_name,
                        n_replicas=n_replicas,n_monomers=n_monomers,
                        use_distance=use_distance,
                        do_soft_core=do_soft_core,
                        ann_timescale=ann_timescale,
                        n_obs_prints=n_obs_prints,
                        n_config_prints=n_config_prints,
                        n_status_prints=20, verbose=2,
                        results_dir=results_dir,
                        hooks_list_init=hooks_list_init,
                        hooks_list_status=hooks_list_status,
                        hooks_list_obs=hooks_list_obs,
                        hooks_list_config=hooks_list_config,
                        additional_energy_list=additional_energy_list,
                        do_slow_init=do_slow_init;
                        kw...);

end;

################################################################################
