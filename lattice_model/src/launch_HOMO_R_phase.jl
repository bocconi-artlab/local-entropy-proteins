using Distributed
@everywhere begin
    #using ProfileView
    using Random
    using Statistics
    using Printf
    using DelimitedFiles
    using ArgParse

    ################################################################################
    include(joinpath(@__DIR__,"replica_sampler.jl"))
    #include("replica_sampler.jl")
    #include("src/replica_sampler.jl")
    MC = LatticePolymerSampler

    ############################################################################

    fix_seed = false

    if fix_seed
        seed = 42
        println("REMINDER: seed fixed to $seed")
        Random.seed!(seed)
    end

    ############################################################################

    n_dim = 3
    n_monomers = 70
    model_name = "HOMO"
    n_replicas = 4

    ann_timescale = 100
    n_mc_steps = Int(1e7)
    n_obs_prints     = 1000
    n_config_prints  = 1000


    do_soft_core = false
    use_distance="common_c"

    use_center=true
    results_dir="../results_phase_"*use_distance

    do_slow_init=true

    #β_list = [1e+9]; Δβ_list = [0.0]; β_max_list = [10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100 150 120 125 130 135 140 145 150 155 160 165 170 175 180 185 190 195]
    β_list = [1e+9]; Δβ_list = [0.0]; β_max_list = [1e-2 2.5e-2 5e-2 1e-1 2.5e-1 5e-1 1 2.5 5 10 20 30 40 50 60 70 80 90 100 110 120 130 140 150 160 170 180 190 200]
    #γ_list = [1e+9]; Δγ_list = [0.0]; γ_max_list = [5e2 1e3 5e3 1e4]
    #γ_list = [1e+9]; Δγ_list = [0.0]; γ_max_list = [ 2e4 4e4 6e4 8e4]
    γ_list = [1e+9]; Δγ_list = [0.0]; γ_max_list = [ 1.5e4 2.4e4 3e4 ]


    n_runs = 1
    run_index_list = collect(1:n_runs)

    ############################################################################

    ## load config and sequence from a previous run
    # reference_dir = "results_reference/HP_3D_N100_R1_B1.0E-02_dB1.3E-02_BM2.0E+02_G0.0E+00_dG0.0E+00_GM0.0E+00/001/config"
    # reference_file = joinpath(reference_dir, "1000000_r1.dat")

    # reference_state = MC.LatticePolymer(n_dim,n_monomers,model_name)
    # ref_configuration, ref_sequence = MC.load_config_and_sequence!(reference_file,reference_state)
    # MC.gen_realization!(reference_state)

    ## compute the distance from a reference configuration
    # dist_from_ref(rsam) = MC.dist_from_ref(reference_state, rsam, do_print=false)
    # overlap_with_ref(rsam) = MC.overlap_with_ref(reference_state, rsam, do_print=false)

    ## pass the ref sequence to rsampler
    sequence = nothing

    ############################################################################

    hooks_list_init=[]
    hooks_list_status=[]
    hooks_list_obs=[]
    hooks_list_config=[]

    ############################################################################
end
#@profview 
pmap([Dict(:run_index=>run_index, :β=>β, :Δβ=>Δβ, :β_max=>β_max, :γ=>γ, :Δγ=>Δγ, :γ_max=>γ_max)
    for run_index in run_index_list
    for  β in  β_list
    for Δβ in Δβ_list
    for  γ in  γ_list
    for Δγ in Δγ_list
    for β_max in β_max_list
    for γ_max in γ_max_list  ## this last cycle is the first to be executed by workers
        ]) do kw

    println(n_monomers," ",kw)

    rsampler = MC.do_monte_carlo( n_mc_steps,
                        sequence=sequence,
                        n_dim=n_dim, model_name=model_name,
                        n_replicas=n_replicas,n_monomers=n_monomers,
                        use_distance=use_distance,
                        use_center=use_center,
                        record_all_replicas=use_center,
                        do_soft_core=do_soft_core,
                        ann_timescale=ann_timescale,
                        n_obs_prints=n_obs_prints,
                        n_config_prints=n_config_prints,
                        n_status_prints=100, verbose=2,
                        results_dir=results_dir,
                        hooks_list_init=hooks_list_init,
                        hooks_list_status=hooks_list_status,
                        hooks_list_obs=hooks_list_obs,
                        hooks_list_config=hooks_list_config,
                        do_slow_init=do_slow_init;
                        kw...);

end;

################################################################################
