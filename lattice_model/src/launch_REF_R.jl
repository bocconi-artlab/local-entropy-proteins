using Distributed
@everywhere begin
    #using ProfileView
    using Random
    using Statistics
    using Printf
    using DelimitedFiles
    using ArgParse

    ################################################################################
    include(joinpath(@__DIR__,"replica_sampler.jl"))
    #include("replica_sampler.jl")
    #include("src/replica_sampler.jl")
    MC = LatticePolymerSampler

    ############################################################################

    fix_seed = false

    if fix_seed
        seed = 42
        println("REMINDER: seed fixed to $seed")
        Random.seed!(seed)
    end

    ############################################################################

    n_dim = 3
    n_monomers = 70
    model_name = "HOMO"
    n_replicas = 4

    ann_timescale = 1000
    n_mc_steps = Int(1e7)
    n_obs_prints     = 1e3
    n_config_prints  = 1e1

    do_soft_core = false
    use_distance="common_c"

    use_center=true
    results_dir="../results_ref_"*use_distance

    do_slow_init=true

    β_list = [1e+9]; Δβ_list = [0.0]; β_max_list = [ 100 ]
    #γ_list = [1e+9]; Δγ_list = [0.0]; γ_max_list = [ 1e4 ]
    #γ_list = [1e+9]; Δγ_list = [0.0]; γ_max_list = [ 5e3 1.5e4 2e4 3e4 ]
    #γ_list = [1e+9]; Δγ_list = [0.0]; γ_max_list = [ 2.5e3 ]
    γ_list = [1e+9]; Δγ_list = [0.0]; γ_max_list = [ 1e-3 ]
    #γ_list = [1e+9]; Δγ_list = [0.0]; γ_max_list = [ 5e2 2e3 3e3 ]
    #γ_list = [1e+9]; Δγ_list = [0.0]; γ_max_list = [ 6e3 7e3 8e3 9e3 1.1e4 1.2e4 1.3e4 1.4e4 ]

    #n_runs = 1
    n_runs = 60
    run_index_list = collect(1:n_runs)

    ############################################################################

    ## pass the ref sequence to rsampler
    sequence = nothing

    ############################################################################

    hooks_list_init=[]
    hooks_list_status=[]
    hooks_list_obs=[]
    hooks_list_config=[]

    ############################################################################
end
#@profview 
pmap([Dict(:run_index=>run_index, :β=>β, :Δβ=>Δβ, :β_max=>β_max, :γ=>γ, :Δγ=>Δγ, :γ_max=>γ_max)
    for run_index in run_index_list
    for  β in  β_list
    for Δβ in Δβ_list
    for  γ in  γ_list
    for Δγ in Δγ_list
    for β_max in β_max_list
    for γ_max in γ_max_list  ## this last cycle is the first to be executed by workers
        ]) do kw

    println(n_monomers," ",kw)

    rsampler = MC.do_monte_carlo( n_mc_steps,
                        sequence=sequence,
                        n_dim=n_dim, model_name=model_name,
                        n_replicas=n_replicas,n_monomers=n_monomers,
                        use_distance=use_distance,
                        use_center=use_center,
                        record_all_replicas=true,
			center_energy=true,
                        do_soft_core=do_soft_core,
                        ann_timescale=ann_timescale,
                        n_obs_prints=n_obs_prints,
                        n_config_prints=n_config_prints,
                        n_status_prints=100, verbose=2,
                        results_dir=results_dir,
                        hooks_list_init=hooks_list_init,
                        hooks_list_status=hooks_list_status,
                        hooks_list_obs=hooks_list_obs,
                        hooks_list_config=hooks_list_config,
                        do_slow_init=do_slow_init;
                        kw...);

end;

################################################################################
