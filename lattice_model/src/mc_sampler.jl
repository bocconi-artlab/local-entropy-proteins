#using ExtractMacro
#using Random

include("lattice_polymer.jl")

################################################################################

mutable struct MCSampler

    n_dim               ::Int64
    n_monomers          ::Int64
    model_name          ::String

    current_state       ::LatticePolymer
    proposed_state      ::LatticePolymer

    β                   ::Float64

    n_move_types        ::Int64
    mv_attempt_counters ::Vector{Int64}
    mv_accept_counters  ::Vector{Int64}

    function MCSampler(n_dim::Int64,n_monomers::Int64, model_name::String, β::Float64; sequence::Union{Vector{Int64},Nothing}=nothing)

        state1 = LatticePolymer(n_dim, n_monomers, model_name, sequence=sequence)
        state2 = deepcopy(state1)

        n_move_types = 3

        mv_attempt_counters = zeros(Float64,n_move_types)
        mv_accept_counters  = zeros(Float64,n_move_types)

        new(n_dim, n_monomers, model_name, state1, state2, β,
            n_move_types, mv_attempt_counters, mv_accept_counters)
    end
end

################################################################################

function restore_proposed_state!(sampler::MCSampler)
    # sampler.proposed_state = deepcopy(sampler.current_state)
    copy_state!(sampler.proposed_state, sampler.current_state)
end

################################################################################
function propose_move!(sampler::MCSampler)
    @extract sampler: n_move_types

    possible_moves = ["f", "c", "r"]
    #selected_move = rand(possible_moves)
    selected_move = rand(1:n_move_types)

    if possible_moves[selected_move] == "f"
        selected_idx = do_flip!(sampler.proposed_state)
    end

    if possible_moves[selected_move] == "c"
        selected_idx = do_crankshaft!(sampler.proposed_state)
    end

    if possible_moves[selected_move] == "r"
        selected_idx = do_rigid_rotation!(sampler.proposed_state)
    end

    gen_config!(sampler.proposed_state)

    return selected_move, selected_idx
end

################################################################################

function accept_move!(sampler::MCSampler)
    copy_state!(sampler.current_state, sampler.proposed_state)
    gen_config!(sampler.current_state)

end

################################################################################

function metropolis_step!(sampler::MCSampler)
    @extract sampler : β n_monomers#, accept_counter

    mv_attempt_counters = sampler.mv_attempt_counters
    mv_accept_counters  = sampler.mv_accept_counters

    max_attempts = 1

    #restore_proposed_state!(sampler)  ## necessary?
    move, idx = propose_move!(sampler)
    accepted = false

    n_attempts = 1
    while !check_good_config(sampler.proposed_state) && (n_attempts < max_attempts)
        move, idx = propose_move!(sampler)
        mv_attempt_counters[move] += 1
        n_attempts += 1
    end

    mv_attempt_counters[move] += 1

    if check_good_config(sampler.proposed_state)

        ## propose metropolis acception rate
        E1 = compute_energy!(sampler.current_state)
        E2 = compute_energy!(sampler.proposed_state)
        ΔE = (E2 - E1)/n_monomers

        #print("ΔE: ",ΔE)

        ## always accept moves that decrease energy
        if ΔE ≤ 0
            accept_move!(sampler)
            mv_accept_counters[move] += 1
            accepted = true
        else
            ## accept moves that increase energy with probabolity p
            p = exp(-β*ΔE)
            r = rand()

            #print(" p: ", p, " r: ", r, "\n")

            if r < p
                accept_move!(sampler)
                mv_accept_counters[move] += 1
                accepted = true
            end
        end

    else
        ## REJECT because of overlaps
        #println("max_attempts: REJECT because of overlaps")
        restore_proposed_state!(sampler)
    end

    return move, idx, accepted
end

################################################################################
