module LatticePolymerSampler

using ExtractMacro
using Random
using LinearAlgebra
using Printf
using DelimitedFiles

using PyCall
rmsd = pyimport("rmsd")

include("mc_sampler.jl")

################################################################################

mutable struct ReplicaSampler

    n_dim               ::Int64
    n_monomers          ::Int64
    model_name          ::String

    β                   ::Float64   ## temperature of the replicas
    n_replicas          ::Int64     ## number of replicas
    γ                   ::Float64   ## scoping parameter of the local entropy

    use_center          ::Bool      ## Changes schemes of couplings between replicas. Center is the first of Y+1 replicas.
    center_energy       ::Bool      ## We may decide to use energy on center if replicas don't collapse

    do_soft_core        ::Bool      ## option to change hard-core repulsion with soft-core repulsion
    ρ                   ::Float64   ## scoping parameter of the soft-core repulsion

    replica             ::Vector{MCSampler}
    sequence            ::Vector{Int64}

    results_dir         ::String

    function ReplicaSampler(n_dim::Int64, n_monomers::Int64, model_name::String,
                                β::Float64, n_replicas::Int64, γ::Float64;
                                sequence::Union{Vector{Int64},Nothing}=nothing,
                                use_center=false, center_energy=true,
                                do_soft_core=false, ρ=1.0)

        replica = Vector{MCSampler}(undef,n_replicas)

        if sequence===nothing
            ## if no sequence is given
            ## initialize the first replica separately,
            ## because we need to copy its sequence to others
            replica[1] = MCSampler(n_dim, n_monomers, model_name, β)
            sequence = replica[1].current_state.sequence
        else
            ## if a sequence is given
            ## use it to initialize the first replica
            replica[1] = MCSampler(n_dim, n_monomers, model_name, β, sequence=sequence)
        end

        ## copy the same sequence into each other replica
        for r in 2:n_replicas
            replica[r] = MCSampler(n_dim, n_monomers, model_name, β, sequence=sequence)
        end

        new(n_dim, n_monomers, model_name, β, n_replicas, γ, use_center, center_energy, do_soft_core, ρ, replica, sequence)
    end

end

################################################################################

function compute_distance(rsampler::ReplicaSampler, r1::Int64, r2::Int64; which_state="current")
    @extract rsampler : replica n_monomers n_dim

    if which_state == "proposed"
        coordinates_r1 = replica[r1].proposed_state.coordinates
        coordinates_r2 = replica[r2].proposed_state.coordinates
    elseif which_state == "current"
        coordinates_r1 = replica[r1].current_state.coordinates
        coordinates_r2 = replica[r2].current_state.coordinates
    end

    dist = 0.0
    @inbounds for m in 1:n_monomers
        coord1, coord2 = @view(coordinates_r1[:,m]), @view(coordinates_r2[:,m])
        dist1 = 0.0
        @simd for d = 1:n_dim
            dist1 += (coord1[d] - coord2[d])^2
        end
        dist += √dist1
    end

    return dist
    #return norm(coordinates_r1 - coordinates_r2)
end

################################################################################

function compute_rmsd(rsampler::ReplicaSampler, r1::Int64, r2::Int64; which_state="current")
    @extract rsampler : replica n_monomers n_dim

    if which_state == "proposed"
        coordinates_r1 = replica[r1].proposed_state.coordinates
        coordinates_r2 = replica[r2].proposed_state.coordinates
    elseif which_state == "current"
        coordinates_r1 = replica[r1].current_state.coordinates
        coordinates_r2 = replica[r2].current_state.coordinates
    end

    return rmsd.kabsch_rmsd(coordinates_r1',coordinates_r2')
    #return rmsd.quaternion_rmsd(coordinates_r1',coordinates_r2')
end

################################################################################

function compute_match_distance(rsampler::ReplicaSampler, r1::Int64, r2::Int64; which_state="current", K=2)
    @extract rsampler : replica n_monomers n_dim

    if which_state=="proposed"
        coordinates_r1 = replica[r1].proposed_state.coordinates
        coordinates_r2 = replica[r2].proposed_state.coordinates
    elseif which_state=="current"
        coordinates_r1 = replica[r1].current_state.coordinates
        coordinates_r2 = replica[r2].current_state.coordinates
    end

    tot_min_dist = 0.0

    for m in 1:n_monomers
        beg_idx = max(1,m-K)
        end_idx = min(n_monomers, m+K)
        range = collect(beg_idx:end_idx)

        min_dist = Inf

        for i1 in range, i2 in range
            #dist = norm(coordinates_r1[:,i1] - coordinates_r2[:,i2])
            coord1, coord2 = @view(coordinates_r1[:,i1]), @view(coordinates_r2[:,i2])
            dist1 = 0.0
            @simd for d = 1:n_dim
                dist1 += (coord1[d] - coord2[d])^2
            end
            dist = √dist1

            if dist < min_dist
                min_dist = dist
            end
        end

        tot_min_dist += min_dist
    end

    return tot_min_dist
end

################################################################################

function compute_common_contacts(rsampler::ReplicaSampler, r1::Int64, r2::Int64; which_state="current", return_max_contacts=false)
    @extract rsampler : replica

    if which_state=="proposed"
        contacts_r1 = find_contacts(replica[r1].proposed_state)
        contacts_r2 = find_contacts(replica[r2].proposed_state)
    elseif which_state=="current"
        contacts_r1 = find_contacts(replica[r1].current_state)
        contacts_r2 = find_contacts(replica[r2].current_state)
    end

    common_contacts = intersect(contacts_r1,contacts_r2)
    n_common_contacts = length(common_contacts)

    if return_max_contacts
        return n_common_contacts, length(contacts_r1), length(contacts_r1)
    else
        return n_common_contacts
    end
end

################################################################################

function compute_matrix_distance(rsampler::ReplicaSampler, r1::Int64, r2::Int64; which_state="current",  dist_to_contact=false)
    @extract rsampler : replica

    if which_state=="proposed"
        d_matrix_r1 = gen_d_matrix(replica[r1].proposed_state, dist_to_contact=dist_to_contact)
        d_matrix_r2 = gen_d_matrix(replica[r2].proposed_state, dist_to_contact=dist_to_contact)
    elseif which_state=="current"
        d_matrix_r1 = gen_d_matrix(replica[r1].current_state, dist_to_contact=dist_to_contact)
        d_matrix_r2 = gen_d_matrix(replica[r2].current_state, dist_to_contact=dist_to_contact)
    end

    return norm(d_matrix_r1 .- d_matrix_r2)
end

################################################################################

#compute_contact_distance(rsampler::ReplicaSampler, r1::Int64, r2::Int64) = compute_matrix_distance(rsampler::ReplicaSampler, r1::Int64, r2::Int64, dist_to_contact=true)

function compute_contact_distance(rsampler::ReplicaSampler, r1::Int64, r2::Int64; which_state="current",max_c::Union{Int64,Nothing}=nothing)
    @extract rsampler : n_monomers

    if max_c===nothing
        max_c = 2 * n_monomers
    end

    (n_common_contacts, n1, n2) = compute_common_contacts(rsampler, r1, r2, which_state=which_state, return_max_contacts=true)
    return 1 - n_common_contacts / max_c
    #return 1 - n_common_contacts/max(n1,n2)
end

################################################################################

function compute_endtoend(rsampler::ReplicaSampler, r1::Int64)
    @extract rsampler : replica n_monomers

    coordinates_r1 = replica[r1].current_state.coordinates
    return norm(coordinates_r1[:,n_monomers])
end

################################################################################

function compute_gyration_radius(rsampler::ReplicaSampler, r1::Int64)
    @extract rsampler : replica

    n_monomers = rsampler.n_monomers

    coordinates_r1 = replica[r1].current_state.coordinates

    gyration_radius = 0.0

    for i in 1:n_monomers
        for j in (i+1):n_monomers
            gyration_radius += norm(coordinates_r1[:,i] - coordinates_r1[:,j])^2
        end
    end

    return sqrt(gyration_radius) / n_monomers
end

################################################################################

function free_step!(rsampler::ReplicaSampler, r1::Int64)
    rsampler.replica[r1].β = 0
    move, idx, accepted = metropolis_step!(rsampler.replica[r1])
    rsampler.replica[r1].β = copy(rsampler.β)
end

################################################################################

function free_metropolis_step!(rsampler::ReplicaSampler, r1::Int64)
    move, idx, accepted = metropolis_step!(rsampler.replica[r1])
end

################################################################################

function coupled_metropolis_step!(rsampler::ReplicaSampler, r1::Int64; use_distance="euclidean", additional_energy_list=[])
    @extract rsampler : β n_replicas n_monomers γ use_center center_energy do_soft_core ρ

    ## select the distance function to use
    if use_distance=="euclidean"
        distance_function = compute_distance
    elseif use_distance=="rmsd"
        distance_function = compute_rmsd
    elseif use_distance=="d_matrix"
        distance_function = compute_matrix_distance
    elseif use_distance=="common_c"
        distance_function = compute_contact_distance
    elseif use_distance=="match"
        distance_function = compute_match_distance
    end

    mv_attempt_counters = rsampler.replica[r1].mv_attempt_counters
    mv_accept_counters  = rsampler.replica[r1].mv_accept_counters

    max_attempts = 1

    restore_proposed_state!(rsampler.replica[r1])  ## necessary? yes
    move, idx = propose_move!(rsampler.replica[r1])
    accepted = false

    n_attempts = 1
    if do_soft_core == false
        while (check_overlaps(rsampler.replica[r1].proposed_state) ≠ 0) && (n_attempts < max_attempts)
            move, idx = propose_move!(rsampler.replica[r1])
            n_attempts += 1
        end
    end

    mv_attempt_counters[move] += 1

    n_overlaps_proposed = check_overlaps(rsampler.replica[r1].proposed_state)

    if n_overlaps_proposed == 0 || do_soft_core

        distance_current = 0.0
        distance_proposed = 0.0
        ###################################################################################################
        if use_center && r1>1

            r2 = 1      ## We set the first replica to be the central one. 
                        ## If this function is called on the first replica,
                        ## we do the other branch of this if and then set beta to zero 
                        ## (actually we skip the computation of E entirely)

            distance_current  += distance_function(rsampler, r1, r2, which_state="current")
            distance_proposed += distance_function(rsampler, r1, r2, which_state="proposed")
        
        else ##############################################################################################
            for r2 in 1:n_replicas
                if (r1 ≠ r2)
                    distance_current  += distance_function(rsampler, r1, r2, which_state="current")
                    distance_proposed += distance_function(rsampler, r1, r2, which_state="proposed")
                end
            end
            distance_current /= n_replicas
            distance_proposed /= n_replicas
        end
        ###################################################################################################

        ## compute soft core additional energy
        if do_soft_core
            n_overlaps_current = check_overlaps(rsampler.replica[r1].current_state)
            ΔE_soft_core = ρ * (n_overlaps_proposed - n_overlaps_current)
        else
            ΔE_soft_core = 0.0
        end

        ## socket for external functions
        ΔE_additional = 0.0
        for additional_energy in additional_energy_list
            ΔE_additional += additional_energy(rsampler,r1)
        end

        ## if we use a central replica and the current one is that one skip the energy 
        if use_center && !center_energy && r1==1
            E1 = 0.0
            E2 = 0.0
        else
            E1 = compute_energy!(rsampler.replica[r1].current_state)
            E2 = compute_energy!(rsampler.replica[r1].proposed_state)
        end

        ## propose metropolis acception rate
        ΔE = E2 - E1 + ΔE_soft_core + ΔE_additional
        #Δdist2 = distance_proposed - distance_current
        Δdist2 = distance_proposed*distance_proposed - distance_current*distance_current

        Δ = (β*ΔE + γ*Δdist2)/(n_monomers)

        # if r1==1
        #     println("$E1 $E2 $(β*ΔE) | $distance_proposed $distance_current $(γ*Δdist2)")
        # end

        ## always accept moves that decrease energy
        if Δ ≤ 0
            accept_move!(rsampler.replica[r1])
            mv_accept_counters[move] += 1
            accepted = true
        else 
            ## Δ > 0
            ## accept moves that increase energy with probabolity p
            p = exp(-Δ)
            r = rand()

            if r < p
                accept_move!(rsampler.replica[r1])
                mv_accept_counters[move] += 1
                accepted = true
            end
        end

    else
        ## REJECT because of overlaps
        restore_proposed_state!(rsampler.replica[r1])
    end

    return move, idx, accepted

end

################################################################################

function do_monte_carlo(n_mc_steps::Int64;
                        sequence::Union{Vector{Int64},Nothing}=nothing,
                        n_monomers=100,             n_replicas=3,
                        β=1.0e-2,                   Δβ=1.3e-3,          β_max=1.0e+1,
                        γ=1.0e-3,                   Δγ=1.0e-3,          γ_max=1.0e+5,
                        n_dim=2,                    model_name="RW",    do_soft_core=false,
                        n_obs_prints=10,            n_config_prints=10,
                        ann_timescale=1,            run_index=0,        ρ=1,
                        use_distance="euclidean",
                        use_center=false,
                        center_energy=true,
                        do_slow_init=true,
                        record_all_replicas = false,
                        n_status_prints=20, verbose=1,
                        results_dir="../results",
                        hooks_list_init=[],
                        hooks_list_obs=[],
                        hooks_list_config=[],
                        hooks_list_status=[],
                        additional_energy_list=[],
                        )

    rsampler = ReplicaSampler(n_dim, n_monomers, model_name, β, n_replicas, γ,
                                use_center=use_center,center_energy=center_energy,
                                do_soft_core=do_soft_core, ρ=ρ, sequence=sequence)

    ## print paths ----------------------------------------------------

    ## if using center start loop from replica 2
    use_center ? r1=2 : r1=1

    global record_index = 1         ## give names to snapshots based on this number instead of mc step
    n_replicas_to_print = 1         ## save only one replica unless record_all_replicas is true
    record_all_replicas && (n_replicas_to_print = n_replicas)

    directory = joinpath(@__DIR__, results_dir)        ## we are inside src folder
    run_name = "$(model_name)_$(n_dim)D_N$(n_monomers)_R$(n_replicas)_B$(@sprintf("%.1E",β))_dB$(@sprintf("%.1E",Δβ))_BM$(@sprintf("%.2E",β_max))_G$(@sprintf("%.1E",γ))_dG$(@sprintf("%.1E",Δγ))_GM$(@sprintf("%.1E",γ_max))"
    run_name = joinpath(run_name, "$(@sprintf("%03d",run_index))")
    observ_dir = joinpath(directory, run_name, "obs")
    mkpath(observ_dir)
    config_dir = joinpath(directory, run_name, "config")
    mkpath(config_dir)

    rsampler.results_dir = joinpath(directory, run_name)

    f_observ = []
    try
        for r in 1:n_replicas_to_print
            filename_observ = "r$(r).dat"
            outfile_observ = joinpath(observ_dir, filename_observ)
            f = open(outfile_observ, "w")
            push!(f_observ, f)

            for hook in hooks_list_init
                hook(rsampler)   ## I might use a macro to avoid this input argument
            end
        end

        ## is do_slow_init==true, do a number of free steps to initialize
        ## the initial configuration to something not necessarly swollen
        if do_slow_init
            n_init_steps = 10000
            println("Doing slow init...")
        else
            n_init_steps = 0
            println("Doing init...")
        end
        
        for init_step in 1:n_init_steps
            ## do a step for each replics
            for r in 1:n_replicas
                free_step!(rsampler,r)
            end
        end
        println("Done init.")

        ## sampling cycle --------------------------------------------------------------
        for step in 1:n_mc_steps

            if n_status_prints*step % n_mc_steps == 0
                print_rsampler_status(rsampler, run_index, step, n_mc_steps, verbose=verbose)
                #println("step\t$step of\t$n_mc_steps β:\t$(@sprintf("%.E",rsampler.β)) γ:\t$(@sprintf("%.E",rsampler.γ))")
                foreach(flush, f_observ)
                for hook in hooks_list_status
                    hook(rsampler,1)   ## I might use a macro to avoid this input argument
                end
            end

            ## update the annealings
            if step % ann_timescale == 0
                rsampler.β = (β_max > 0) ? min(rsampler.β*(1+Δβ), β_max) : rsampler.β*(1+Δβ)
                rsampler.γ = (γ_max > 0) ? min(rsampler.γ*(1+Δγ), γ_max) : rsampler.γ*(1+Δγ)
            end

            ## do a step for each replics
            for r in 1:n_replicas
                #println("$step $r")
                move, idx, accepted = coupled_metropolis_step!(rsampler,r,use_distance=use_distance,
                                                                          additional_energy_list=additional_energy_list)
                #println(move, idx, accepted)
            end

            ## compute observables
            if n_obs_prints*step % n_mc_steps == 0
                for r in 1:n_replicas_to_print
                    n_contacts = length(find_contacts(rsampler.replica[r].current_state))
                    compute_energy!(rsampler.replica[r].current_state)
                    energy = rsampler.replica[r].current_state.energy
                    # endtoend = compute_endtoend(rsampler, r)
                    gyration_radius = compute_gyration_radius(rsampler, r)
                    n_overlapping_monomers = check_overlaps(rsampler.replica[r].current_state)

                    accept_rate_1 = rsampler.replica[r].mv_accept_counters[1] #/ rsampler.replica[r].mv_attempt_counters[1]
                    accept_rate_2 = rsampler.replica[r].mv_accept_counters[2] #/ rsampler.replica[r].mv_attempt_counters[2]
                    accept_rate_3 = rsampler.replica[r].mv_accept_counters[3] #/ rsampler.replica[r].mv_attempt_counters[3]

                    ###########################################################################################
                    if r==1
                        distances = 0.0
                        overlaps  = 0.0
                        rms_dist  = 0.0

                        for r2 in 2:n_replicas
                            distances += compute_distance(rsampler, r, r2, which_state="current")
                            overlaps  += compute_contact_distance(rsampler, r, r2, which_state="current")
                            rms_dist  += compute_rmsd(rsampler, r, r2, which_state="current")
                        end

                        av_dist = distances / (n_replicas-1)
                        av_overlap = overlaps / (n_replicas-1)
                        av_rms_dist = rms_dist / (n_replicas-1)
                    ###########################################################################################
                    else
                        r2 = 1
                        av_dist = compute_distance(rsampler, r, r2, which_state="current")
                        av_overlap = compute_contact_distance(rsampler, r, r2, which_state="current")
                        av_rms_dist = compute_rmsd(rsampler, r, r2, which_state="current")
                    end
                    ###########################################################################################

                    ## print observables
                    println(f_observ[r], "$(step) $(n_contacts) $(n_overlapping_monomers) $(energy) $(av_dist) $(av_overlap) $(av_rms_dist) $(gyration_radius) $(accept_rate_1) $(accept_rate_2) $(accept_rate_3) $(rsampler.β) $(rsampler.γ)")
                                          #1       2            3                         4          5             6           7                  8                9                10                  11          12              13
                    for hook in hooks_list_obs
                        r == 1 && hook(rsampler,r)
                    end
                end
            end

            ## print config
            if n_config_prints*step % n_mc_steps == 0
                for r in 1:n_replicas_to_print
                    filename_config = "$(@sprintf("%05d", record_index))_r$(r).dat"
                    outfile_config = joinpath(config_dir, filename_config)
                    config = gen_config!(rsampler.replica[r].current_state)
                    config_with_types = hcat(config', rsampler.sequence)
                    writedlm(outfile_config, config_with_types, ' ')
                    for hook in hooks_list_config
                        r == 1 && hook(rsampler,r)   ## I might use a macro to avoid this input argument
                    end
                end
                record_index += 1
            end
        end
    finally
        foreach(close, f_observ)
    end

    return rsampler
end
################################################################################

function print_rsampler_status(rsampler::ReplicaSampler, run_index, step, n_mc_steps; verbose=1)
    if verbose==0
        return nothing

    elseif verbose==1
        energy = rsampler.replica[1].current_state.energy
        println("run $run_index step $step of $n_mc_steps \tE: $(@sprintf("%.E", energy))")
        return nothing

    elseif verbose==2
        energy = rsampler.replica[1].current_state.energy
        println("run $run_index step $step of $n_mc_steps \tE: $(@sprintf("%.E", energy)) \tβ: $(@sprintf("%.E",rsampler.β)) \tγ: $(@sprintf("%.E",rsampler.γ))")
        return nothing
    end
end
################################################################################

include("hooks.jl")

end  # module
