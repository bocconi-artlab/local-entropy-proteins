
############################################################################
function my_hook()
    println("hello hook")
end

function my_hook(a::Int64)
    println("hello hook $a")
end
############################################################################
function do_loop(n_steps::Int64; hooks_list=[])
    for i in 1:n_steps
        println(i)
        for hook in hooks_list
            hook()
        end
    end

    return
end
############################################################################
function do_loop_2(n_steps::Int64; hooks_list=[])
    for i in 1:n_steps
        println(i)
        for hook in hooks_list
            hook(i)
        end
    end

    return
end
############################################################################
h1() = my_hook()
h2() = my_hook(4)
h3() = my_hook(5)

do_loop(3,hooks_list=[h1,h2,h3])

h4(a) = my_hook(a)

do_loop_2(3,hooks_list=[h4])

#
