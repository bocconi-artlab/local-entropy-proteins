#!/bin/bash

IN_DIR="./pdb"
#IN_DIR="./pdb_test"
OUT_DIR="./pdb_go"
TEMP_DIR="./pdb_temp"

## clean
#rm -r $OUT_DIR
rm -r $TEMP_DIR

mkdir $OUT_DIR
mkdir $TEMP_DIR

## make list of files to cycle on 
#FILES=$(ls $IN_DIR)

# FILES="
# 1pgb.pdb
# "

# FILES="
# 1qm0.pdb
# 2kdl.pdb
# 2kdm.pdb
# "


# FILES="
# 6ha1_Y.pdb
# 6ha1_Z.pdb
# 6ha1_0.pdb
# 6ha1_1.pdb
# 6ha1_2.pdb
# 6ha1_3.pdb
# 6ha1_4.pdb
# 5ib7_G5.pdb
# "


# FILES="
# 1sfx.A.pdb
# 1q05.A.pdb
# 5ngm.A4.pdb
# 6ha1.Y.pdb
# 6ha1.Z.pdb
# 6ha1.0.pdb
# 6ha1.1.pdb
# 6ha1.2.pdb
# 6ha1.3.pdb
# 6ha1.4.pdb
# 5ib7.G5.pdb
# 5ib7.H5.pdb
# 2l8n.A.pdb
# 6wmr.E.pdb
# 2kj3.A.pdb
# 2xks.A.pdb
# 2xku.A.pdb
# "

# FILES="
# 1pdb.pdb
# 2abd.pdb
# 2ci2.pdb
# EEHEE_rd4_0094.pdb
# EHEE_rd4_0009.pdb
# HEEH_rd4_0053.pdb
# HHH_rd4_0231.pdb
# "
FILES="
1srl.pdb
"

# FILES="
# 2xku_1_fixed.pdb
# 2xks_1_fixed.pdb
# "

for FILE in $FILES; do
    ## if I want i can rm the extension with
    ## ${FILE%.pdb}
    ## then add something like _temp.pbd
    ## before saving the files in pdb_go
    NAME=${FILE%.pdb}
    echo $NAME

    ## remove anything that smog2 does not like, that means keeping 
    ## ATOM HETATM COMMENT TER END
    ## remove also HETATM because they describe non-polymeric ligands
    ## remove hydrogens
    ## select first (A) chain (removes TER)
    #   if this causes problems with the naming of the chain, the script pdb_chainbows can be useful
    
    pdb_keepcoord $IN_DIR/$FILE | pdb_delhetatm | pdb_delelem -H | \
                                  pdb_chainbows | pdb_selchain -A > $TEMP_DIR/$NAME.clean.pdb

    #pdb_keepcoord $IN_DIR/$FILE | pdb_delelem -H  > $TEMP_DIR/$NAME.clean.pdb
    #pdb_chainbows $IN_DIR/$FILE > $TEMP_DIR/$NAME.clean.pdb

    #pdb_keepcoord | pdb_delelem -H $IN_DIR/$FILE > $TEMP_DIR/$NAME.clean.pdb

    ## remove MODEL and ENDMDL lines, that angers smog2
    ## remove TER lines because TER immediately followed by END angers smog2
    sed -i "" '/^REMARK/d' $TEMP_DIR/$NAME.clean.pdb
    sed -i "" '/^MODEL/d' $TEMP_DIR/$NAME.clean.pdb
    sed -i "" '/^ENDMDL/d' $TEMP_DIR/$NAME.clean.pdb
    sed -i "" '/^TER/d' $TEMP_DIR/$NAME.clean.pdb
    sed -i "" '/^CONECT/d' $TEMP_DIR/$NAME.clean.pdb

    ## use smog2 tool to set the proper formatting for smog2 itself
    smog_adjustPDB -i $TEMP_DIR/$NAME.clean.pdb -o $TEMP_DIR/$NAME.adj.pdb
    #cat $TEMP_DIR/$NAME.clean.pdb > $TEMP_DIR/$NAME.adj.pdb

    # sometimes the file omits END at the end and this angers grom2
    # check if END is there, if not add it
    grep -qxF -- "END" "$TEMP_DIR/$NAME.adj.pdb" || echo 'END' >> $TEMP_DIR/$NAME.adj.pdb

    # finally use smog2 to create the go-model files
    smog2 -i $TEMP_DIR/$NAME.adj.pdb -AA -g $OUT_DIR/$NAME.box.gro \
                                         -o $OUT_DIR/$NAME.top \
                                         -s $OUT_DIR/$NAME.contacts \
                                         -n $OUT_DIR/$NAME.ndx \
                                         -backup no

    ## change box size to a big one, the same for every run
    #awk -F ' ' 'NF!=3{print}; NF==3{print "   15.0000   15.0000   15.0000"}' $OUT_DIR/$NAME.box.gro > $OUT_DIR/$NAME.gro
    awk -F ' ' 'NF!=3{print}; NF==3{print "   30.0000   30.0000   30.0000"}' $OUT_DIR/$NAME.box.gro > $OUT_DIR/$NAME.gro

    ## clean 
    rm $OUT_DIR/$NAME.box.gro
 
done