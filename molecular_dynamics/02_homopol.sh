#!/bin/bash

DIR_GO="./pdb_go"

# STRUCTURES="
# 1pgb
# 2abd
# 2ci2
# HHH_rd4_0231
# EHEE_rd4_0009
# EEHEE_rd4_0094
# HEEH_rd4_0053
# "

# STRUCTURES="
# 1qm0
# 2kdl
# 2kdm 
# 6ha1_Y
# 6ha1_Z
# 6ha1_0
# 6ha1_1
# 6ha1_2
# 6ha1_3
# 6ha1_4
# 5ib7_G5
# "

# STRUCTURES="
# 2xku_1_fixed
# 2xks_1_fixed
# "
CONTACT_STEP=4

A_LIST="1.0e-02 1.4e-02 2.0e-02"

for NAME in $STRUCTURES; do
    for A in $A_LIST; do

        TYPE="1"
        #A="1.4e-02"
        B="1.0e-04"

        IDX=$A

        INFILE=$DIR_GO/$NAME.top
        OUTFILE=${INFILE%.top}.$IDX.homopol.top

        echo $OUTFILE
        rm $OUTFILE
        ########################################################################

        ## save everything in the ATOMS section to extract NATOMS
        BEG="\[ atoms \]"
        END="\[ bonds \]"

        sed -n "/^$BEG/,/^$END/p" $INFILE > temp_atoms.txt

        #NATOMS=$( tail -3 temp_atoms.txt | head -1 | awk '{print $1}')
        NATOMS=$( tail -3 temp_atoms.txt | head -1 | awk '{print $1}')
        #NATOMS=$(ehco $INFILE | awk '{print $1}')
        #NATOMS="5"     ## debug purpose
        echo $NATOMS

        rm temp_atoms.txt

        ########################################################################

        ## delete everything in the DIHEDRALS section
        BEG="\[ dihedrals \]"
        END="\[ pairs \]"

        sed "/^$BEG/,/^$END/{/^$BEG/!{/^$END/!d;};}" $INFILE > $OUTFILE

        ########################################################################

        ## delete everything in the PAIRS section
        BEG="\[ pairs \]"
        END="\[ exclusions \]"

        sed -i "" "/^$BEG/,/^$END/{/^$BEG/!{/^$END/!d;};}" $OUTFILE

        ## print new section to temp file    
        for (( ai=1; ai<=$NATOMS; ai+=$CONTACT_STEP )); do
            for (( aj=ai+1; aj<=$NATOMS; aj+=$CONTACT_STEP )); do
                echo $ai $aj $TYPE $A $B >> tmp.txt
            done
        done

        ## add text after section title
        sed -i "" "/^$BEG/r ./tmp.txt" $OUTFILE

        rm tmp.txt

        ########################################################################

        ## delete everything in the EXCLUSIONS section
        BEG="\[ exclusions \]"
        END="\[ system \]"

        sed -i "" "/^$BEG/,/^$END/{/^$BEG/!{/^$END/!d;};}" $OUTFILE

        ## print new section to temp file
        for (( ai=1; ai<=$NATOMS; ai+=$CONTACT_STEP )); do
            for (( aj=ai+1; aj<=$NATOMS; aj+=$CONTACT_STEP )); do
                echo $ai $aj >> tmp.txt
            done
        done

        ## add text after section title
        sed -i "" "/^$BEG/r ./tmp.txt" $OUTFILE

        rm tmp.txt

        ########################################################################

    done
done