
. parameters.homopol.sh

for RUN_NAME in $STRUCTURES; do
    for T in $TEMPERATURES; do

        ## run no more than MAX_PARALLEL processes 
        ## each one is using THREADS cores
        ((i=i%MAX_PARALLEL)); ((i++==0)) && wait

        DIR=runs_optimiz/$RUN_NAME/T$T
        
        rm -r $DIR
        mkdir -p $DIR
        cp $MDP_FILE $DIR/$MDP_FILE

        GRO=${RUN_NAME%%.*}.gro
        #GRO=${RUN_NAME%.homopol}.gro
        TOP=$RUN_NAME.top
        TPR=$RUN_NAME.tpr

        echo $GRO
        echo $TOP
        echo $TPR

        ## print the desired parametes in the run settings
        sed -i "" "s/nsteps.*/nsteps = $NSTEPS/g" $DIR/$MDP_FILE
        sed -i "" "s/ref_t.*/ref_t = $T/g"        $DIR/$MDP_FILE
        sed -i "" "s/gen_temp.*/gen_temp = $T/g"  $DIR/$MDP_FILE

        ## set energy minimination
        sed -i "" "s/integrator.*/integrator = steep/g" $DIR/$MDP_FILE

        ## prepare the input file for gromacs
        gmx grompp -f $DIR/$MDP_FILE -c $PATH2GO/$GRO -p $PATH2GO/$TOP -o $DIR/$TPR -maxwarn 1 -backup no #& #> $DIR/grompp.out 2> $DIR/grompp.err &

        ## run the energy minimization 
        gmx mdrun -nt $THREADS -deffnm $DIR/$RUN_NAME -s $DIR/$TPR -backup no -noddcheck & #> $DIR/mdrun.out 2> $DIR/mdrun.err &
    done
done
