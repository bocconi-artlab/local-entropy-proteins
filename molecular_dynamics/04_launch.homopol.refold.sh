
#. parameters.homopol.sh

# STRUCTURES="
# 1bnr.1.4e-02.homopol
# 1cye.1.4e-02.homopol
# 5vnt.1.4e-02.homopol
# "
STRUCTURES="
1srl.1.4e-02.homopol
"

MDP_FILE="settings.mdp"
#PATH2GO="pdb_go"
PATH2GO="extra_pdb_go"

## useful just for refolding and maybe local entropy sampling
TEMPERATURES="300"
THREADS=1
MAX_PARALLEL=14
#MAX_PARALLEL=4
NSTEPS=1000000

#SAMPLE_IDXS="00"
SAMPLE_IDXS="01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19"
#SAMPLE_IDXS="10 11 12 13 14 15 16 17 18 19"
#SAMPLE_IDXS="10"
#SAMPLE_IDXS="10 11"


for RUN_NAME in $STRUCTURES; do
    for T in $TEMPERATURES; do
        for IDX in $SAMPLE_IDXS; do
            
            ## run no more than MAX_PARALLEL processes 
            ## each one is using THREADS cores
            ((i=i%MAX_PARALLEL)); ((i++==0)) && wait            

            DIR=runs_refold/$RUN_NAME/T$T
            
            #rm -r $DIR
            mkdir -p $DIR
            cp $MDP_FILE $DIR/$MDP_FILE

            ## now we use the energy-optimized gro as initial configuration
            GRO=runs_optimiz/$RUN_NAME/T$T/$RUN_NAME.gro
            TOP=$RUN_NAME.top
            TPR=$RUN_NAME.tpr

            ## print the desired parametes in the run settings
            sed -i "" "s/nsteps.*/nsteps = $NSTEPS/g" $DIR/$MDP_FILE
            sed -i "" "s/ref_t.*/ref_t = $T/g"        $DIR/$MDP_FILE
            sed -i "" "s/gen_temp.*/gen_temp = $T/g"  $DIR/$MDP_FILE

            echo $DIR/$MDP_FILE

            ## set simulated annealing
            echo "" >> $DIR/$MDP_FILE
            echo "" >> $DIR/$MDP_FILE
            echo "annealing = single" >> $DIR/$MDP_FILE
            echo "annealing-npoints = 3" >> $DIR/$MDP_FILE
            echo "annealing-time =   0 200 400" >> $DIR/$MDP_FILE
            #echo "annealing-time =   0 200 220" >> $DIR/$MDP_FILE
            echo "annealing-temp = 300 300  10" >> $DIR/$MDP_FILE

            ## prepare the input file for gromacs
            gmx grompp -f $DIR/$MDP_FILE -c $GRO -p $PATH2GO/$TOP -o $DIR/$TPR -maxwarn 1 -backup no #> $DIR/grompp.out 2> $DIR/grompp.err &

            ## run the molecular (dynamics using checkpoints? -cpt)
            gmx mdrun -nt $THREADS -s $DIR/$TPR -deffnm $DIR/$RUN_NAME.$IDX -backup no -noddcheck & # $DIR/mdrun.out 2> $DIR/mdrun.err &

        done
    done
done
