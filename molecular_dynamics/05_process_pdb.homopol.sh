#!/bin/bash

IN_DIR="extra_pdb_homopol"
OUT_DIR="extra_pdb_homopol_go"
# IN_DIR="pdb_homopol"
# OUT_DIR="pdb_homopol_go"
TEMP_DIR="pdb_temp"

## clean
# rm -r $IN_DIR
# rm -r $OUT_DIR
# rm -r $TEMP_DIR

mkdir $IN_DIR
mkdir $OUT_DIR
mkdir $TEMP_DIR

BASE_LIST="
1srl
"

# BASE_LIST="
# 1bnr
# 1cye
# 1srl
# 5vnt
# "

for BASE in $BASE_LIST; do

# NAMES="
# $BASE.1.4e-02.homopol.01
# "
NAMES="
$BASE.1.4e-02.homopol.00
$BASE.1.4e-02.homopol.01
$BASE.1.4e-02.homopol.02
$BASE.1.4e-02.homopol.03
$BASE.1.4e-02.homopol.04
$BASE.1.4e-02.homopol.05
$BASE.1.4e-02.homopol.06
$BASE.1.4e-02.homopol.07
$BASE.1.4e-02.homopol.08
$BASE.1.4e-02.homopol.09
$BASE.1.4e-02.homopol.10
$BASE.1.4e-02.homopol.11
$BASE.1.4e-02.homopol.12
$BASE.1.4e-02.homopol.13
$BASE.1.4e-02.homopol.14
$BASE.1.4e-02.homopol.15
$BASE.1.4e-02.homopol.16
$BASE.1.4e-02.homopol.17
$BASE.1.4e-02.homopol.18
$BASE.1.4e-02.homopol.19
"


## set up smog2 layout and create go model
for NAME in $NAMES; do

    ## extract the name of the structure with its run index
    # NAME=${FILE%.gro}
    # NAME=${NAME#$RUN_DIR/}

    echo $NAME

    RUN_DIR="runs_refold/${NAME%.*}/T300"

    IN_GRO=$RUN_DIR/$NAME.gro
    OUT_PDB=$IN_DIR/$NAME.pdb

    echo $IN_GRO
    echo $OUT_PDB

    # convert gro to pdb and save into pdb_homopol
    gmx editconf -f $IN_GRO -o $OUT_PDB -backup no

    ## remove anything that smog2 does not like, that means keeping 
    ## ATOM HETATM COMMENT TER END
    ## remove also HETATM because they describe non-polymeric ligands
    ## remove hydrogens
    ## select first (A) chain (removes TER)
    ##   if this causes problems with the naming of the chain, the script pdb_chainbows can be useful
    pdb_keepcoord $OUT_PDB | pdb_delhetatm | pdb_delelem -H | \
                                      pdb_chainbows | pdb_selchain -A > $TEMP_DIR/$NAME.clean.pdb

    ## remove MODEL and ENDMDL lines, that angers smog2
    ## remove TER lines because TER immediately followed by END angers smog2
    sed -i "" '/^MODEL/d' $TEMP_DIR/$NAME.clean.pdb
    sed -i "" '/^ENDMDL/d' $TEMP_DIR/$NAME.clean.pdb
    sed -i "" '/^TER/d' $TEMP_DIR/$NAME.clean.pdb

    ## remove OC2 and change OC1 to "O  "
    sed -i "" "s/OC1/O\ \ /g" $TEMP_DIR/$NAME.clean.pdb
    sed -i "" '/OC2/d' $TEMP_DIR/$NAME.clean.pdb

    ## use smog2 tool to set the proper formatting for smog2 itself
    #cat $TEMP_DIR/$NAME.clean.pdb > $TEMP_DIR/$NAME.adj.pdb
    smog_adjustPDB -i $TEMP_DIR/$NAME.clean.pdb -o $TEMP_DIR/$NAME.adj.pdb


    ## sometimes the file omits END at the end and this angers grom2
    ## check if END is there, if not add it
    grep -qxF -- "END" "$TEMP_DIR/$NAME.adj.pdb" || echo 'END' >> $TEMP_DIR/$NAME.adj.pdb

    # finally use smog2 to create the go-model files
    smog2 -i $TEMP_DIR/$NAME.adj.pdb -AA \
                                        -limitbondlength \
                                         -g $OUT_DIR/$NAME.box.gro \
                                         -o $OUT_DIR/$NAME.top \
                                         -s $OUT_DIR/$NAME.contacts \
                                         -n $OUT_DIR/$NAME.ndx \
                                         -backup no
                                         

    ## change box size to a big one, the same for every run
    #awk -F ' ' 'NF!=3{print}; NF==3{print "   15.0000   15.0000   15.0000"}' $OUT_DIR/$NAME.box.gro > $OUT_DIR/$NAME.gro
    awk -F ' ' 'NF!=3{print}; NF==3{print "   100.0000   100.0000   100.0000"}' $OUT_DIR/$NAME.box.gro > $OUT_DIR/$NAME.initial.gro

    #echo $OUT_DIR/$NAME.initial.gro

    ## clean 
    rm $OUT_DIR/$NAME.box.gro
 
done

rm -r $TEMP_DIR

wait
echo "\nFIRST PHASE DONE\n"

# optimize go model to create the actual reference state. This is needed because random go has LJ clashes and appears to be glassy
THREADS=1
MAX_PARALLEL=16
MDP_FILE="settings.mdp"

for NAME in $NAMES; do
    ((i=i%MAX_PARALLEL)); ((i++==0)) && wait
    cp $MDP_FILE $OUT_DIR/$MDP_FILE

    echo $OUT_DIR

    T=0.01
    NSTEPS=100000
    ## print the desired parametes in the run settings
    sed -i "" "s/nsteps.*/nsteps = $NSTEPS/g" $OUT_DIR/$MDP_FILE
    sed -i "" "s/ref_t.*/ref_t = $T/g" $OUT_DIR/$MDP_FILE
    sed -i "" "s/gen_temp.*/gen_temp = $T/g" $OUT_DIR/$MDP_FILE

    ################################################################################################################
    ## set energy minimination
    sed -i "" "s/integrator.*/integrator = steep/g" $OUT_DIR/$MDP_FILE

    ## prepare the input file for gromacs
    gmx grompp -f $OUT_DIR/$MDP_FILE -c $OUT_DIR/$NAME.initial.gro  -p $OUT_DIR/$NAME.top -o $OUT_DIR/$NAME.tpr -maxwarn 1 -backup no #& #> $DIR/grompp.out 2> $DIR/grompp.err &

    ## run the energy minimization 
    gmx mdrun -nt $THREADS -deffnm $OUT_DIR/$NAME  -c $OUT_DIR/$NAME.final.gro -backup no -noddcheck #& #> $DIR/mdrun.out 2> $DIR/mdrun.err &

done

wait
echo "\nSECOND PHASE DONE\n"

# now we run smog2 again to build a model around our actual reference
for NAME in $NAMES; do
    #((i=i%MAX_PARALLEL)); ((i++==0)) && wait        #########??????

    echo $NAME

    IN_GRO=$OUT_DIR/$NAME.final.gro
    OUT_PDB=$OUT_DIR/$NAME.final.pdb

    # convert gro to pdb and save into pdb_homopol
    gmx editconf -f $IN_GRO -o $OUT_PDB -backup no
    
    # adjust stuff for smog2
    sed -i "" '/^TITLE/d' $OUT_PDB
    sed -i "" '/^REMARK/d' $OUT_PDB
    sed -i "" '/^CRYST1/d' $OUT_PDB
    sed -i "" '/^MODEL/d' $OUT_PDB
    sed -i "" '/^ENDMDL/d' $OUT_PDB
    sed -i "" '/^TER/d' $OUT_PDB
    grep -qxF -- "END" $OUT_PDB || echo 'END' >> $OUT_PDB

    # finally use smog2 to create the go-model files
    smog2 -i $OUT_PDB -AA                \
                                        -limitbondlength \
                                         -g $OUT_DIR/$NAME.box.gro \
                                         -o $OUT_DIR/$NAME.top \
                                         -s $OUT_DIR/$NAME.contacts \
                                         -n $OUT_DIR/$NAME.ndx \
                                         -backup no
                                         
    ## change box size to a big one, the same for every run
    #awk -F ' ' 'NF!=3{print}; NF==3{print "   30.0000   30.0000   30.0000"}' $OUT_DIR/$NAME.box.gro > $OUT_DIR/$NAME.initial.gro
    awk -F ' ' 'NF!=3{print}; NF==3{print "   30.0000   30.0000   30.0000"}' $OUT_DIR/$NAME.box.gro > $OUT_DIR/$NAME.gro

    ## clean 
    rm $OUT_DIR/$NAME.box.gro
done

done
