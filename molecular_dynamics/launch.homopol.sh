
#. parameters.sh
#. parameters2debug.sh

# STRUCTURES="
# 1pgb.1.4e-02.homopol.00
# 1pgb.1.4e-02.homopol.01
# 1pgb.1.4e-02.homopol.02
# 1pgb.1.4e-02.homopol.03
# 1pgb.1.4e-02.homopol.04
# 1pgb.1.4e-02.homopol.05
# 1pgb.1.4e-02.homopol.06
# 1pgb.1.4e-02.homopol.07
# 1pgb.1.4e-02.homopol.08
# 1pgb.1.4e-02.homopol.09
# 1pgb.1.4e-02.homopol.10
# 1pgb.1.4e-02.homopol.11
# 1pgb.1.4e-02.homopol.12
# 1pgb.1.4e-02.homopol.13
# 1pgb.1.4e-02.homopol.14
# 1pgb.1.4e-02.homopol.15
# 1pgb.1.4e-02.homopol.16
# 1pgb.1.4e-02.homopol.17
# 1pgb.1.4e-02.homopol.18
# 1pgb.1.4e-02.homopol.19
# 2abd.1.4e-02.homopol.00
# 2abd.1.4e-02.homopol.01
# 2abd.1.4e-02.homopol.02
# 2abd.1.4e-02.homopol.03
# 2abd.1.4e-02.homopol.04
# 2abd.1.4e-02.homopol.05
# 2abd.1.4e-02.homopol.06
# 2abd.1.4e-02.homopol.07
# 2abd.1.4e-02.homopol.08
# 2abd.1.4e-02.homopol.09
# 2abd.1.4e-02.homopol.10
# 2abd.1.4e-02.homopol.11
# 2abd.1.4e-02.homopol.12
# 2abd.1.4e-02.homopol.13
# 2abd.1.4e-02.homopol.14
# 2abd.1.4e-02.homopol.15
# 2abd.1.4e-02.homopol.16
# 2abd.1.4e-02.homopol.17
# 2abd.1.4e-02.homopol.18
# 2abd.1.4e-02.homopol.19
# HHH_rd4_0231.1.4e-02.homopol.00
# HHH_rd4_0231.1.4e-02.homopol.01
# HHH_rd4_0231.1.4e-02.homopol.02
# HHH_rd4_0231.1.4e-02.homopol.03
# HHH_rd4_0231.1.4e-02.homopol.04
# HHH_rd4_0231.1.4e-02.homopol.05
# HHH_rd4_0231.1.4e-02.homopol.06
# HHH_rd4_0231.1.4e-02.homopol.07
# HHH_rd4_0231.1.4e-02.homopol.08
# HHH_rd4_0231.1.4e-02.homopol.09
# HHH_rd4_0231.1.4e-02.homopol.10
# HHH_rd4_0231.1.4e-02.homopol.11
# HHH_rd4_0231.1.4e-02.homopol.12
# HHH_rd4_0231.1.4e-02.homopol.13
# HHH_rd4_0231.1.4e-02.homopol.14
# HHH_rd4_0231.1.4e-02.homopol.15
# HHH_rd4_0231.1.4e-02.homopol.16
# HHH_rd4_0231.1.4e-02.homopol.17
# HHH_rd4_0231.1.4e-02.homopol.18
# HHH_rd4_0231.1.4e-02.homopol.19
# "

# STRUCTURES="
# 2ci2.1.4e-02.homopol.00
# 2ci2.1.4e-02.homopol.01
# 2ci2.1.4e-02.homopol.02
# 2ci2.1.4e-02.homopol.03
# 2ci2.1.4e-02.homopol.04
# 2ci2.1.4e-02.homopol.05
# 2ci2.1.4e-02.homopol.06
# 2ci2.1.4e-02.homopol.07
# 2ci2.1.4e-02.homopol.08
# 2ci2.1.4e-02.homopol.09
# 2ci2.1.4e-02.homopol.10
# 2ci2.1.4e-02.homopol.11
# 2ci2.1.4e-02.homopol.12
# 2ci2.1.4e-02.homopol.13
# 2ci2.1.4e-02.homopol.14
# 2ci2.1.4e-02.homopol.15
# 2ci2.1.4e-02.homopol.16
# 2ci2.1.4e-02.homopol.17
# 2ci2.1.4e-02.homopol.18
# 2ci2.1.4e-02.homopol.19
# "
# PATH2GO="pdb_homopol_go"

# STRUCTURES="
# 1pgb.1.4e-02.homopol.00
# 1pgb.1.4e-02.homopol.01
# 1pgb.1.4e-02.homopol.02
# 1pgb.1.4e-02.homopol.03
# 1pgb.1.4e-02.homopol.04
# 1pgb.1.4e-02.homopol.05
# 1pgb.1.4e-02.homopol.06
# 1pgb.1.4e-02.homopol.07
# 1pgb.1.4e-02.homopol.08
# 1pgb.1.4e-02.homopol.09
# 1pgb.1.4e-02.homopol.10
# 1pgb.1.4e-02.homopol.11
# 1pgb.1.4e-02.homopol.12
# 1pgb.1.4e-02.homopol.13
# 1pgb.1.4e-02.homopol.14
# 1pgb.1.4e-02.homopol.15
# 1pgb.1.4e-02.homopol.16
# 1pgb.1.4e-02.homopol.17
# 1pgb.1.4e-02.homopol.18
# 1pgb.1.4e-02.homopol.19
# "
# PATH2GO="pdb_homopol_go"

STRUCTURES="
1srl.1.4e-02.homopol.00
1srl.1.4e-02.homopol.01
1srl.1.4e-02.homopol.02
1srl.1.4e-02.homopol.03
1srl.1.4e-02.homopol.04
1srl.1.4e-02.homopol.05
1srl.1.4e-02.homopol.06
1srl.1.4e-02.homopol.07
1srl.1.4e-02.homopol.08
1srl.1.4e-02.homopol.09
1srl.1.4e-02.homopol.10
1srl.1.4e-02.homopol.11
1srl.1.4e-02.homopol.12
1srl.1.4e-02.homopol.13
1srl.1.4e-02.homopol.14
1srl.1.4e-02.homopol.15
1srl.1.4e-02.homopol.16
1srl.1.4e-02.homopol.17
1srl.1.4e-02.homopol.18
1srl.1.4e-02.homopol.19
"
PATH2GO="extra_pdb_homopol_go"
#PATH2GO="pdb_homopol_go"

#TEMPERATURES="1 1.1 1.3 1.5 2 2.5 3 3.5"
#TEMPERATURES="1 1.1 1.3 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7 7.5 8 8.5 9 9.5 10 10.5 11"
#TEMPERATURES="20 30 40 50 60 70 80 90 100 120 140 160 180 200"
#TEMPERATURES="13 16 20 23 26 30 33 36 110"
#TEMPERATURES="1 1.1 1.3 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7 7.5 8 8.5 9 9.5 10 10.5 11 13 16 20 23 26 30 33 36 40 50 60 70 80 90 100 110 120 140 160 180 200"
#TEMPERATURES="1.05 1.2 1.4 1.7 2.2 2.7 3.2 3.7 4.2 4.7 5.2 5.7 6.2 6.7 7.2 7.7 8.2 8.7 9.2 9.7 10.2 10.7 \
#              12.5 14.5 17.5 21.5 24.5 27.5 31.5 34.5 37.5 45 55 65 75 85 95 105 115 130 150 170 190"
# TEMPERATURES="1 1.1 1.3 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7 7.5 8 8.5 9 9.5 10 10.5 11 13 16 20 23 26 30 33 36 40 50 60 70 80 90 100 110 120 140 160 180 200 \
#               1.05 1.2 1.4 1.7 2.2 2.7 3.2 3.7 4.2 4.7 5.2 5.7 6.2 6.7 7.2 7.7 8.2 8.7 9.2 9.7 10.2 10.7 \
#               12.5 14.5 17.5 21.5 24.5 27.5 31.5 34.5 37.5 45 55 65 75 85 95 105 115 130 150 170 190"
#TEMPERATURES="260 270 280 290"
#TEMPERATURES="210 220 230 240 250 260 270 280 290 300"
#TEMPERATURES="310 320 330 340 350 360 370 380 390 400"
#TEMPERATURES="131 132 133 134 135 136 137 138 139"
#TEMPERATURES=" 131 132 133 134 135 136 137 138 139 210 220 230 240 250 260 270 280 290 300"
TEMPERATURES="
            1 1.05 1.1 1.2 1.3 1.4 1.5 1.7
            2 2.2 2.5 2.7 3 3.2 3.5 3.7 
            4 4.2 4.5 4.7 5 5.2 5.5 5.7 6 6.2 6.5 6.7 7 7.2 7.5 7.7
            8 8.2 8.5 8.7 9 9.2 9.5 9.7 10 10.2 10.5 10.7 
            11 12.5 13 14.5 16 17.5 20 21.5
            23 24.5 26 27.5 30 31.5
            33 34.5 36 37.5 40 
            45 50 55 60 65 70 75 80 85 90 95 100 105
            110 115 120 130 
            131 132 133 134 135 136 137 138 139
            132 135 138 
            140 150 160 170 180 190 200
            210 220 230 240 250 260 270 280 290 300
"
MDP_FILE="settings.mdp"

## set general parameters that are common to all subdirectories 
THREADS=1
MAX_PARALLEL=14
NSTEPS=200000
#NSTEPS=400000

# NSTEPS=1000000

# for RUN_NAME in $STRUCTURES; do
#     for T in $TEMPERATURES; do
#         ((i=i%MAX_PARALLEL)); ((i++==0)) && wait

#         OUTDIR=runs_test/$RUN_NAME/T$T
        
#         rm -r $OUTDIR
#         mkdir -p $OUTDIR
#         cp $MDP_FILE $OUTDIR/$MDP_FILE

#         T=1
#         ## print the desired parametes in the run settings
#         sed -i "" "s/nsteps.*/nsteps = $NSTEPS/g" $OUTDIR/$MDP_FILE
#         sed -i "" "s/ref_t.*/ref_t = $T/g" $OUTDIR/$MDP_FILE
#         sed -i "" "s/gen_temp.*/gen_temp = $T/g" $OUTDIR/$MDP_FILE

#         ################################################################################################################
#         ## set energy minimination
#         sed -i "" "s/integrator.*/integrator = steep/g" $DIR/$MDP_FILE

#         ## prepare the input file for gromacs
#         gmx grompp -f $OUTDIR/$MDP_FILE -c $PATH2GO/$RUN_NAME.gro  -p $PATH2GO/$RUN_NAME.top -o $OUTDIR/$RUN_NAME.tpr -maxwarn 1 -backup no #& #> $DIR/grompp.out 2> $DIR/grompp.err &

#         ## run the energy minimization 
#         gmx mdrun -nt $THREADS -deffnm $OUTDIR/$RUN_NAME -backup no -noddcheck  & #> $DIR/mdrun.out 2> $DIR/mdrun.err &

#      done
# done

# wait



for RUN_NAME in $STRUCTURES; do
    for T in $TEMPERATURES; do
        ((i=i%MAX_PARALLEL)); ((i++==0)) && wait

        OUTDIR=runs/$RUN_NAME/T$T
        
        rm -r $OUTDIR
        mkdir -p $OUTDIR
        cp $MDP_FILE $OUTDIR/$MDP_FILE

        ## print the desired parametes in the run settings
        sed -i "" "s/nsteps.*/nsteps = $NSTEPS/g" $OUTDIR/$MDP_FILE
        sed -i "" "s/ref_t.*/ref_t = $T/g" $OUTDIR/$MDP_FILE
        sed -i "" "s/gen_temp.*/gen_temp = $T/g" $OUTDIR/$MDP_FILE
        sed -i "" "s/nstenergy.*/nstenergy = 500/g" $OUTDIR/$MDP_FILE

        ################################################################################################################
        ## set molecular dynamics 
        sed -i "" "s/integrator.*/integrator = sd/g" $DIR/$MDP_FILE

        ## prepare the input file for gromacs
        #gmx grompp -f $OUTDIR/$MDP_FILE -c $OUTDIR/$RUN_NAME.gro -p $PATH2GO/$RUN_NAME.top -o $OUTDIR/$RUN_NAME.tpr -maxwarn 1 -backup no #> $OUTDIR/grompp.out 2> $OUTDIR/grompp.err &
        gmx grompp -f $OUTDIR/$MDP_FILE -c $PATH2GO/$RUN_NAME.gro  -p $PATH2GO/$RUN_NAME.top -o $OUTDIR/$RUN_NAME.tpr -maxwarn 1 -backup no #& #> $DIR/grompp.out 2> $DIR/grompp.err &

        ## run the molecular dynamics 
        gmx mdrun -nt $THREADS -deffnm $OUTDIR/$RUN_NAME -backup no -noddcheck > $OUTDIR/mdrun.out 2> $OUTDIR/mdrun.err &
    done
done