
STRUCTURES="
1srl
"

PATH2GO="pdb_go"

TEMPERATURES="
            1 1.05 1.1 1.2 1.3 1.4 1.5 1.7
            2 2.2 2.5 2.7 3 3.2 3.5 3.7 
            4 4.2 4.5 4.7 5 5.2 5.5 5.7 6 6.2 6.5 6.7 7 7.2 7.5 7.7
            8 8.2 8.5 8.7 9 9.2 9.5 9.7 10 10.2 10.5 10.7 
            11 12.5 13 14.5 16 17.5 20 21.5
            23 24.5 26 27.5 30 31.5
            33 34.5 36 37.5 40 
            45 50 55 60 65 70 75 80 85 90 95 100 105
            110 115 120 130 
            131 132 133 134 135 136 137 138 139
            132 135 138 
            140 150 160 170 180 190 200
            210 220 230 240 250 260 270 280 290 300
"

MDP_FILE="settings.mdp"

THREADS=1
MAX_PARALLEL=1

NSTEPS=200000

for RUN_NAME in $STRUCTURES; do
    for T in $TEMPERATURES; do
        ((i=i%MAX_PARALLEL)); ((i++==0)) && wait

        OUTDIR=runs/$RUN_NAME/T$T
        
        rm -r $OUTDIR
        mkdir -p $OUTDIR
        cp $MDP_FILE $OUTDIR/$MDP_FILE

        ## print the desired parametes in the run settings
        sed -i "" "s/nsteps.*/nsteps = $NSTEPS/g" $OUTDIR/$MDP_FILE
        sed -i "" "s/ref_t.*/ref_t = $T/g" $OUTDIR/$MDP_FILE
        sed -i "" "s/gen_temp.*/gen_temp = $T/g" $OUTDIR/$MDP_FILE
        sed -i "" "s/nstenergy.*/nstenergy = 500/g" $OUTDIR/$MDP_FILE

        ## prepare the input file for gromacs
        gmx grompp -f $OUTDIR/$MDP_FILE -c $PATH2GO/$RUN_NAME.gro -p $PATH2GO/$RUN_NAME.top -o ./$OUTDIR/$RUN_NAME.tpr -maxwarn 1 -backup no #> $OUTDIR/grompp.out 2> $OUTDIR/grompp.err &

        echo $PATH2GO/$RUN_NAME.gro

        ## run the molecular dynamics 
        gmx mdrun -nt $THREADS -deffnm $OUTDIR/$RUN_NAME -backup no -noddcheck > $OUTDIR/mdrun.out 2> $OUTDIR/mdrun.err &
    done
done
