import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from cycler import cycler
import sys
from matplotlib.patches import Rectangle

######################################################################################

# cm0=cm.rainbow_r
# nc = 2
# plt.rc('axes', prop_cycle=(cycler('color', [cm0(c/(nc-1)) for c in range(nc)])))

######################################################################################

def load_energy_traj(path, cut_idx=0, every=1, col=1, c=('#','@','&')):

    energy_traj = np.loadtxt(path+"energy.xvg",comments=c)[::every,:]
    energy_traj = energy_traj[cut_idx::,col]

    return energy_traj

def fetch_native_energy(filename):

    base = filename[2:-4]
    path = f"../runs/{base}/T1/"
    energy_traj = load_energy_traj(path)

    print(base, energy_traj[0])

    return energy_traj[0]

def fetch_transition_energy(filename, path=""):

    data = np.loadtxt(path+filename[2:])
    temp = data[:,0]
    energy = data[:,1]
    Cv = data[:,3]

    ## find critical temperature
    swapped_Cv = Cv[::-1]
    diff_swapped_Cv = swapped_Cv[1:] - swapped_Cv[:-1]
    first_peak_swapped_idx = np.argmax(diff_swapped_Cv<=0)
    first_peak_idx = Cv.shape[0] - first_peak_swapped_idx
    first_peak_temp = temp[first_peak_idx]

    first_peak_energy = energy[first_peak_idx]

    return first_peak_energy, first_peak_temp

######################################################################################
names = (
("1pgb"        ,56,[1,3,4,6,8,10,12,13], -6.0, 10, -0.80, 0.00, True, None),
("2abd"        ,86,[13]                , -6.0, 10, -0.80, 0.00, True, None),
("2ci2"        ,83,[1,2,16,17]         , -6.0, 10, -0.80, 0.00, True, None),
("HHH_rd4_0231",43,[5]                 , -6.0, 10, -0.80, 0.00, True, None),
("1bnr"        ,110,[0,11,4]           , -6.0, 10, -0.80, 0.00, True, None),
("1cye"        ,129,[6,7,11,14,18]     , -6.0, 10, -0.80, 0.00, True, None),
("5vnt"        ,63, [12]               , -6.0, 10, -0.80, 0.00, True, None),
("1srl"        ,56, [3,6,14,18]        , -6.0, 10, -0.80, 0.00, True, None),
)

for n,items in enumerate(names):
    #ax = fig.add_subplot(221+n)
    fig = plt.figure(figsize=(4,3))

    name, N, ignore, xmin, xmax, ymin, ymax, scale_energy, subnumber = items


    filenames=(
    f"g_{name}.dat", ## this is needed first for analysis
    f"g_{name}.1.4e-02.homopol.00.dat",
    f"g_{name}.1.4e-02.homopol.01.dat",
    f"g_{name}.1.4e-02.homopol.02.dat",
    f"g_{name}.1.4e-02.homopol.03.dat",
    f"g_{name}.1.4e-02.homopol.04.dat",
    f"g_{name}.1.4e-02.homopol.05.dat",
    f"g_{name}.1.4e-02.homopol.06.dat",
    f"g_{name}.1.4e-02.homopol.07.dat",
    f"g_{name}.1.4e-02.homopol.08.dat",
    f"g_{name}.1.4e-02.homopol.09.dat",
    f"g_{name}.1.4e-02.homopol.10.dat",
    f"g_{name}.1.4e-02.homopol.11.dat",
    f"g_{name}.1.4e-02.homopol.12.dat",
    f"g_{name}.1.4e-02.homopol.13.dat",
    f"g_{name}.1.4e-02.homopol.14.dat",
    f"g_{name}.1.4e-02.homopol.15.dat",
    f"g_{name}.1.4e-02.homopol.16.dat",
    f"g_{name}.1.4e-02.homopol.17.dat",
    f"g_{name}.1.4e-02.homopol.18.dat",
    f"g_{name}.1.4e-02.homopol.19.dat",
    f"g_{name}.dat", ## this is needed last to appear on the top in the plot
    )

    colors=(
    "C0",
    "C1",
    )

    labels=(
    f"{name} RAND",
    f"{name} NAT",
    )

    styles = ("--","-")
    linewidths = (0.5,2)

    last=len(filenames)-1

    transition_energies = []
    transition_temperatures = []

    for i,filename in enumerate(filenames):

        if i-1 in ignore: continue
        #########################################################
        style=styles[0]
        if i==last: style=styles[1]

        label=labels[0]
        if i==last: label=labels[1]

        color=colors[0]
        if i==last: color=colors[1]

        lw=linewidths[0]
        if i==last: lw=linewidths[1]
        #########################################################

        data = np.loadtxt("results/"+filename)
        energy = data[:,0]
        entropy = data[:,1]

        #########################################################
        ## filter out spikes 
        filter_zeros = entropy != 0
        energy = energy[filter_zeros]
        entropy = entropy[filter_zeros]

        ## filter out tails of random decoys
        if i < 21 and i > 0:
            energy = energy[:-10]
            entropy = entropy[:-10]

        ## set max entropy to zero
        x = (energy)/N
        y = (entropy)/N
        y = (entropy - entropy.max())/N

        ## scale curves so that origins coincide
        if scale_energy:
            if i == 0:
                x_nat = x[0]
            x_dec = x[0]
            x_scale = x_nat/ x_dec
        else:
            x_scale = 1

        ## draw lines up to the max entropy value
        y_max_idx = np.argmax(y)
        x = x[:y_max_idx+1]
        y = y[:y_max_idx+1]
        ###########################################
        
        ## main plot with zoom in
        ax = fig.add_subplot(111)
        ax.plot(x*x_scale,y, style, label=label, color=color, linewidth=lw)
        ax.set_xlabel("Go-Model Energy Density E/N")
        ax.set_ylabel("Entropy Density S(E)/N")     
        ax.set_ylim([ymin, ymax])
        ax.grid(linewidth=0.1)

        ## inset with zoom out
        left, bottom, width, height = [0.60, 0.30, 0.30, 0.30]
        ax2 = fig.add_axes([left, bottom, width, height])
        ax2.plot(x*x_scale,y, style, label=label, color=color, linewidth=lw/2)
        ax2.set_xticks([0,10,20])
        ax2.set_yticks([0,-0.4,-0.8])
        ax2.grid(linewidth=0.1)

        ###########################################
        transition_energy, transition_temperature = fetch_transition_energy(filename, path="results/")
        rescaled_transition_energy = transition_energy*x_scale/N

        transition_energies.append(rescaled_transition_energy)
        transition_temperatures.append(transition_temperature)

        #print(filename, rescaled_transition_energy, transition_temperature)

        #fetch_native_energy(filename)

    ######################################################################################

    transition_energies = np.array(transition_energies)
    av_tr_en = transition_energies.mean()
    std_tr_en = transition_energies.std()

    transition_temperatures = np.array(transition_temperatures)
    av_tr_temp = transition_temperatures.mean()
    std_tr_temp = transition_temperatures.std()

    print(name, av_tr_en, std_tr_en, av_tr_temp, std_tr_temp)
    np.savetxt(f"transition_{name}.txt", np.array([av_tr_en, std_tr_en]))

    rect_x0 = av_tr_en - std_tr_en
    rect_x_width = 2 * std_tr_en
    rect_y0 = -0.8
    rect_y_width = 0.8

    rect_y_width = 0.8
    rect1 = Rectangle((rect_x0,rect_y0),rect_x_width,rect_y_width,facecolor="gray", alpha=0.2)
    ax.add_patch(rect1)
    rect2 = Rectangle((rect_x0,rect_y0),rect_x_width,rect_y_width,facecolor="gray", alpha=0.2)
    ax2.add_patch(rect2)

    ax.set_xlim([xmin*x_scale, av_tr_en])   

    plt.tight_layout()
    plt.subplots_adjust(top=0.90)
    ax.text(-9.0, 0.0, subnumber,fontsize=18,weight="bold")

    if name == "HHH_rd4_0231": name = "HHH"
    plt.suptitle(name)

    plt.savefig(f"images/entropy_{name}.pdf")
    plt.close()
