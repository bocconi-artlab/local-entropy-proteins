import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from cycler import cycler
from scipy import stats

from numpy.core.fromnumeric import mean

######################################################################################

# cm0=cm.rainbow_r
# nc = 2
# plt.rc('axes', prop_cycle=(cycler('color', [cm0(c/(nc-1)) for c in range(nc)])))

######################################################################################

def load_energy_traj(path, cut_idx=0, every=1, col=1, c=('#','@','&')):

    energy_traj = np.loadtxt(path+"energy.xvg",comments=c)[::every,:]
    energy_traj = energy_traj[cut_idx::,col]

    return energy_traj

def fetch_native_energy(name):

    path = f"../runs/{name}/T1/"
    energy_traj = load_energy_traj(path)

    return energy_traj[0]

def gaussian(x, mu=0, sigma=1):
    return np.exp(-(x - mu)*(x - mu)/(2*sigma*sigma))/(np.sqrt(2*np.pi*sigma*sigma))

######################################################################################
fig, ax1 = plt.subplots(figsize=(4,3))
ax2 = ax1.twinx()

######################################################################################
names = (

("1cye"        , 129,[6,7,11,14,18]     , -6.0, 10, -0.80, 0.00, 10, -0.06, True),
("2ci2"        , 83,[1,2,16,17]         , -6.0, 10, -0.80, 0.00, 10, -0.06, True),
("1bnr"        , 110,[0,11,4]           , -6.0, 10, -0.80, 0.00, 10, -0.06, True),
("2abd"        , 86,[13]                , -6.0, 10, -0.80, 0.00, 10, -0.06, True),
("5vnt"        , 63, [12]               , -6.0, 10, -0.80, 0.00, 10, -0.06, True),
("1srl"        , 56, [3,6,14,18]        , -6.0, 10, -0.80, 0.00, 10, -0.06, True),
("1pgb"        , 56,[1,3,4,6,8,10,12,13], -6.0, 10, -0.80, 0.00, 10, -0.06, True),
("HHH_rd4_0231", 43,[5]                 , -6.0, 10, -0.80, 0.00, 10, -0.06, True),
)

p_values = []
for n,items in enumerate(names):
    #ax = fig.add_subplot(221+n)
    #fig = plt.figure(figsize=(4,3))

    name, N, ignore, xmin, xmax, ymin, ymax, range_cut, peak, scale_energy = items

    print(n, name)

    filenames=(
    f"g_{name}.dat", ## this is needed first for analysis
    f"g_{name}.1.4e-02.homopol.00.dat",
    f"g_{name}.1.4e-02.homopol.01.dat",
    f"g_{name}.1.4e-02.homopol.02.dat",
    f"g_{name}.1.4e-02.homopol.03.dat",
    f"g_{name}.1.4e-02.homopol.04.dat",
    f"g_{name}.1.4e-02.homopol.05.dat",
    f"g_{name}.1.4e-02.homopol.06.dat",
    f"g_{name}.1.4e-02.homopol.07.dat",
    f"g_{name}.1.4e-02.homopol.08.dat",
    f"g_{name}.1.4e-02.homopol.09.dat",
    f"g_{name}.1.4e-02.homopol.10.dat",
    f"g_{name}.1.4e-02.homopol.11.dat",
    f"g_{name}.1.4e-02.homopol.12.dat",
    f"g_{name}.1.4e-02.homopol.13.dat",
    f"g_{name}.1.4e-02.homopol.14.dat",
    f"g_{name}.1.4e-02.homopol.15.dat",
    f"g_{name}.1.4e-02.homopol.16.dat",
    f"g_{name}.1.4e-02.homopol.17.dat",
    f"g_{name}.1.4e-02.homopol.18.dat",
    f"g_{name}.1.4e-02.homopol.19.dat",
    f"g_{name}.dat", ## this is needed last to appear on the top in the plot
    )

    colors=(
    "C0",
    "C1",
    )

    labels=(
    f"{name} RAND",
    f"{name} NAT",
    )

    styles = ("--","-")
    linewidths = (0.5,2)

    last=len(filenames)-1

    local_entropies = []

    for i,filename in enumerate(filenames):

        if i-1 in ignore: continue

        data = np.loadtxt("results/"+filename)
        energy = data[:,0]
        entropy = data[:,1]

        transition_data = np.loadtxt("results/"+f"transition_{name}.txt")
        av_tr_en, std_tr_en = transition_data[0], transition_data[1]

        #########################################################
        ## filter out spikes 
        filter_zeros = entropy != 0
        energy = energy[filter_zeros]
        entropy = entropy[filter_zeros]
        ## filter out tails of random decoys
        if i < 21 and i > 0:
            energy = energy[:-10]
            entropy = entropy[:-10]
        #########################################################

        style=styles[0]
        if i==last: style=styles[1]

        label=labels[0]
        if i==last: label=labels[1]

        color=colors[0]
        if i==last: color=colors[1]

        lw=linewidths[0]
        if i==last: lw=linewidths[1]

        x = (energy)
        y = (entropy)
        y = (entropy - entropy.max())

        ## scale curves so that origins coincide
        if scale_energy:
            if i == 0:
                x_nat = x[0]
            x_dec = x[0]
            x_scale = x_nat/ x_dec
        else:
            x_scale = 1

        ###########################################

        ## the transition energy is already divided by N
        transition_idx = np.argmin(x/N < av_tr_en)
        x = x[:transition_idx+1]
        y = y[:transition_idx+1]
        
        #print(x.shape, y.shape)
        #print(x @ y)
        #print(np.diff(x))
        #delta_E = np.diff(x)[0]

        #local_entropy = (x @ y)/N
        #local_entropy = y.sum()*delta_E/N

        n_en_bins = x.shape[0]
        beta = 1/10
        summation = 0
        for k in range(n_en_bins):
            #print(energy[k], entropy[k])
            summation += np.exp(-beta*x[k]+y[k])

        local_entropy = np.log(summation)/N

        #print(filename, local_entropy)

        if i==0 or i==21:
        #if filename==f"g_{name}.dat":
            marker = "x"
            color = "C1"
        else:
            marker = "."
            color = "C0"

        label=None
        if n==0:
            if i==21:
            # if filename==f"g_{name}.dat":
                label="native"
            #if filename==f"g_{name}.1.4e-02.homopol.00.dat" and i==0:
            if i==20:
                label="random"
            
        if i < 20 and i > 0:
            local_entropies.append(local_entropy)
        if i == 20: 
            le_mean = np.array([local_entropies]).mean()
            le_std = np.array([local_entropies]).std()
            ax1.errorbar(le_mean, n, xerr=le_std, fmt=marker,markersize=10,color=color,label=label)
            ax2.errorbar(le_mean, n, xerr=le_std, fmt=marker,markersize=10,color=color,label=label)
        if i == 21:
            ax1.plot(local_entropy, n, marker,markersize=10,color=color,label=label)
            ax2.plot(local_entropy, n, marker,markersize=10,color=color,label=label)

            ## p-value
            p_value = stats.norm.sf(local_entropy,loc=le_mean, scale=le_std)
            p_values.append("{:.4f}".format(p_value))
            #print(p_value)
            #ax1.text(-0.1, n-0.25, "{:.3f}".format(p_value),fontsize=10,color="gray")
            #plt.text(local_entropy+0.02, n-0.25, "{:.3f}".format(p_value),fontsize=12,color="gray")
        
    ######################################################################################

mod_names = np.array([name[0] for name in names])
mod_names[mod_names=="HHH_rd4_0231"]="HHH"
#print(mod_names)

ax1.set_yticks(ticks=range(8))
ax1.set_yticklabels(mod_names)
ax1.legend(loc='lower right' )

ax2.set_yticks(ticks=range(8))
ax2.set_yticklabels(np.array(p_values))
ax2.tick_params(width=0)

ax2.text(0.1655, 7.7, "p-values:",fontsize=8,weight="bold")

ax1.set_xlabel("Density of Local Entropy $s_{loc}$")


plt.tight_layout()
plt.subplots_adjust(top=0.90)
plt.savefig("images/entropy_sum.pdf")
