import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import sys

T_list = [  
            1, 1.05, 1.1, 1.2, 1.3, 1.4, 1.5, 1.7, 
            2, 2.2, 2.5, 2.7, 3, 3.2, 3.5, 3.7, 
            4, 4.2, 4.5, 4.7, 5, 5.2, 5.5, 5.7, 6, 6.2, 6.5, 6.7, 7, 7.2, 7.5, 7.7,
            8, 8.2, 8.5, 8.7, 9, 9.2, 9.5, 9.7, 10, 10.2, 10.5, 10.7, 
            11, 12.5, 13, 14.5, 16, 17.5, 20, 21.5,
            23, 24.5, 26, 27.5, 30, 31.5,
            33, 34.5, 36, 37.5, 40, 
            45, 50, 55, 60, 65, 70, 75, 80, 85 ,90, 95, 100, 105,
            110, 115, 120, 130, 
            131, 132, 133, 134, 135, 136, 137, 138, 139,
            132, 135, 138, 
            140, 150, 160, 170, 180, 190, 200,
            210, 220, 230, 240, 250, 
            260, 270, 280, 290, 
            300,
            ]

ntemp = len(T_list)

filename="mh_in_template.txt"

outfile = open(filename, 'w')

print(f"""ntemp {ntemp}

files""", file=outfile)

for T in T_list:
    print(f"../runs/NAME/T{T}/energy_cut.dat    {T}     1 ",file=outfile)

print("""
kb 1
tmin 1.0
tbin 0.1
ntbin 3000

nohisto

outfile results/NAME.dat
gfile results/g_NAME.dat

debug 1
""",file=outfile)


outfile.close()